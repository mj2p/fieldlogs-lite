﻿;Installation File for FieldLogs Lite
SetWorkingDir \My Documents\

version = 1.1

;Tell the user what is going on
MsgBox, 36, FieldLogs Lite, You are about to install FieldLogs Lite %version% on your device.`n`nWould you like to continue?
IfMsgBox, No
{
	Return
}

If !FileExist("\My Documents\FieldLogs Lite\")
{
	FileCreateDir, \My Documents\FieldLogs Lite\
	If ErrorLevel
	{
		Gui, Destroy
		MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create Document Directory;'
		Return
	}
}

;Create a progress bar so they can see that stuff is going on
Gui, -Caption +Border
Gui, Add, Text, w220 x10 y50, Now Installing FieldLogs Lite
Gui, Add, Text, w220 x10 y100 vAction, Checking Directories
Gui, Add, Text, w220 x10 y120 vProgress,
Gui, Show, x0 y23 w240 h300, FieldLogs Lite Install

;Hide the soft keyboard window
DllCall("SipShowIM", "UInt", 0) ;Hide
WinHide, MS_SIPBUTTON

;The progress bar isn't a true progress bar as they don't work properly on CE.
;Instead it is a text box which we fill with right arrows
GuiControl, , Progress, >

;Create the Program directory if it doesn't exist 
If !FileExist("\Program Files\FieldLogs Lite\")
{
	;Create the installation directory afresh 
	FileCreateDir, \Program Files\FieldLogs Lite\
	;and end if there's a problem
	If ErrorLevel
	{
		Gui, Destroy
		MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create Program Dir ;'
		Return
	}
}

;Update the progress bar
GuiControl, , Progress, >>

;Update the GUI to tell the user what we are doing
GuiControl, , Action, Installing Files

;Install the Icon
FileInstall, \My Documents\FieldLogs Lite Install\log.ico, \Program Files\FieldLogs Lite\log.ico, 1

;progress again
GuiControl, , Progress, >>>

;Create the settings.ini file, quit if there was a problem
FileInstall, \My Documents\FieldLogs Lite Install\settings.ini, \Program Files\FieldLogs Lite\settings.ini, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create settings file ;'
	Return
}

GuiControl, , Progress, >>>>

;Install the program files. we keep them as ahk files as compiling the script causes errors. 
;It's a bug but this version of AHK isn't in active development so this is a workaround
FileInstall, \My Documents\FieldLogs Lite Install\FieldLogs Lite.ahk, \Program Files\FieldLogs Lite\FieldLogs Lite.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>
FileInstall, \My Documents\FieldLogs Lite Install\functions.ahk, \Program Files\FieldLogs Lite\functions.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\hands.ahk, \Program Files\FieldLogs Lite\hands.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\historic.ahk, \Program Files\FieldLogs Lite\historic.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\mainui.ahk, \Program Files\FieldLogs Lite\mainui.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\paramdata.ahk, \Program Files\FieldLogs Lite\paramdata.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\refdata.ahk, \Program Files\FieldLogs Lite\refdata.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\settings.ahk, \Program Files\FieldLogs Lite\settings.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}
GuiControl, , Progress, >>>>>>>>>>>>
FileInstall, \My Documents\FieldLogs Lite Install\tbrdata.ahk, \Program Files\FieldLogs Lite\tbrdata.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't install FieldLog System File ;'
	Return
}

GuiControl, , Progress, >>>>>>>>>>>>>

;Install the cab file which allows us to instyall AHKCE on the device if needed
FileInstall, \My Documents\FieldLogs Lite Install\AutoHotkeyCEUni.CAB, \Program Files\FieldLogs Lite\AutoHotkeyCEUni.CAB, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't copy AHK.cab
	Return
}

;Show the user what's going on
GuiControl, , Progress, >>>>>>>>>>>>>>

GuiControl, , Action, Creating Shortcuts

;Remove old shortcuts and files
;old versions ofthis installer seemed to create duplicate files in the Start menu and program folders
;this routine is here to clear up after them to avoid confusern

FileDelete, \Windows\Programs\FieldLogs*
FileDelete, \Windows\Start Menu\FieldLogs*
FileDelete, \Windows\Start Menu\Programs\FieldLogs*

GuiControl, , Progress, >>>>>>>>>>>>>>>

;Copy the script to here so that it shows up in the Programs List on the device
FileCopy, \Program Files\FieldLogs Lite\FieldLogs Lite.ahk, \Windows\Programs\FieldLogs Lite.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create Programs shortcut ;'
	Return
}

GuiControl, , Progress, >>>>>>>>>>>>>>>>

;Copy the script there so that it shows up in the start menu on the device
FileCopy, \Program Files\FieldLogs Lite\FieldLogs Lite.ahk, \Windows\Start Menu\FieldLogs Lite.ahk, 1
If ErrorLevel
{
	Gui, Destroy
	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create Start Menu shortcut ;'
	Return
}

GuiControl, , Progress, >>>>>>>>>>>>>>>>>

;One last time, copy it here so that it shows up as a pinned program in the start menu.
;FileCopy, \Program Files\FieldLogs Lite\FieldLogs Lite.ahk, \Windows\Start Menu\Programs\FieldLogs Lite.ahk, 1
;If ErrorLevel
;{
;	Gui, Destroy
;	MsgBox, 16, FieldLogs Lite, There was an error.`nPlease try again`nCouldn't create Start Menu Programs shortcut ;'
;	Return
;}

;Tell the user what's going on
GuiControl, , Progress, >>>>>>>>>>>>>>>>>>

GuiControl, , Action, Checking Run Environment

Sleep, 1500

GuiControl, , Progress, >>>>>>>>>>>>>>>>>>>

;Destory the GUI
Gui, Destroy

;To let the user see that the progress bar is full we pause for a second. otherwise the GUI appears and flashes off straight away
Sleep, 1000
;Check if AHKCE is already installed on the device
If !FileExist("\Program Files\AutoHotkeyCE\AutoHotkeyCE.exe")
{
	;If it isn't we tell the user that we need to insatll it and give some basic instructions
	MsgBox, 64, FieldLogs Lite, FieldLogs Lite requires AutoHotkeyCE to be installed in order to run correctly.`nPlease "OK" the next installer.  
	;When they click OK we run the installer .cab file
	RunWait, \Program Files\FieldLogs Lite\AutoHotkeyCEUni.CAB
}
Else
{
	;Tell the user that everything went OK and tell them how to access the app from their start menu
	MsgBox, 64, Field Logs Lite, Installation was completed successfully.`nYou can run FieldLogs Lite from your Start Menu
}
;Quit
ExitApp
