FieldLogs-Lite
==============

FieldLogs Lite

A field data collection tool for use in Hydrometry and Telemetry.

The input and output files are for use with the Field Logs database system.

Written in AutoHotkey <http://autohotkey.com>, this program will compile either for PC or for Windows CE

