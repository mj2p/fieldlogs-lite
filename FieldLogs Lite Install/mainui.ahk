﻿;#####################################################################################################################
;#####################################################################################################################
;Once the user has chosen a site and set the Risk Assessment we allow them to collect data based on the site type

ButtonOK:

;##################################
;First we need to get them to assign reasons to their visit

Gui, 30: -Caption +Border
Gui, 30: Font, s10
Gui, 30: Add, Text, w220 x10 y10 Center, Please indicate the reasons for your visit
;These are saved as boolean values
Gui, 30: Font, s9
Gui, 30: Add, Checkbox, % "w150 x50 y50 vReason_QA gCheck_Reasons Checked" . (Reason_QA ? 1 : 0), QA Visit
Gui, 30: Add, Checkbox, % "w150 x50 y70 vReason_Gauging gCheck_Reasons Checked" . (Reason_Gauging ? 1 : 0), Gauging
Gui, 30: Add, Checkbox, % "w150 x50 y90 vReason_Fault_Fix gCheck_Reasons Checked" . (Reason_Fault_Fix ? 1 : 0), Fault Fix
Gui, 30: Add, Checkbox, % "w150 x50 y110 vReason_Weir_Clean gCheck_Reasons Checked" . (Reason_Weir_Clean ? 1 : 0), Weir Clean
Gui, 30: Add, Checkbox, % "w150 x50 y130 vReason_Survey gCheck_Reasons Checked" . (Reason_Survey ? 1 : 0), Levelling/Surveying
Gui, 30: Add, Checkbox, % "w150 x50 y150 vReason_Battery_Change gCheck_Reasons Checked" . (Reason_Battery_Change ? 1 : 0), Battery Change
Gui, 30: Add, Checkbox, % "w150 x50 y170 vReason_HS gCheck_Reasons Checked" . (Reason_HS ? 1 : 0), Health and Safety
Gui, 30: Add, Checkbox, % "w150 x50 y190 vReason_Maintenance gCheck_Reasons Checked" . (Reason_Maintenance ? 1 : 0), Maintenance Visit
Gui, 30: Add, Checkbox, % "w150 x50 y210 vReason_Well_Flush gCheck_Reasons Checked" . (Reason_Well_Flush ? 1 : 0), Well Flush
Gui, 30: Add, Checkbox, % "w150 x50 y230 vReason_Calibration gCheck_Reasons Checked" . (Reason_Calibaration ? 1 : 0), Instrument Calibration

Gui, 30: Add, Button, w60 x10 y270 h20, Back
Gui, 30: Add, Button, w60 x170 y270 h20 Disabled vReasons_Next_Button, Next

GoSub, Check_Reasons

Gui, 30: Show, x0 y23 w240 h300, Visit Reasons
SIPControl(0)
Return

;We only enable the 'Next' button if at least one reason has been checked
Check_Reasons:
Gui, 30: Submit, NoHide
Reasons := "Reason_QA|Reason_Gauging|Reason_Fault_Fix|Reason_Weir_Clean|Reason_Survey|Reason_Battery_Change|Reason_HS|Reason_Maintenance|Reason_Well_Flush|Reason_Calibration"
Loop, Parse, Reasons, |
{
	If (%A_LoopField%)
	{
		GuiControl, 30: Enable, Reasons_Next_Button
		Return
	}
}
GuiControl, 30: Disable, Reasons_Next_Button
Return

;If the back button is pressed we don't save anything and re-enable the main UI
30ButtonBack:
Gui, 30: Destroy
If (SaveDetails)
{
	Gui, 10: Default
	Gui, 10: Show
}
Else
{	
	Gui, 1: Default
	Gui, 1: Show
}
SIPControl(0)
Return

;If the next buitton is pressed we submit the main UI to get the status of the H&S of the site
30ButtonNext:
Gui, 30: Submit
Gui, 30: Destroy
Gui, 1: Submit, NoHide
If (SaveDetails)
{
	SaveDetails = 0
	Return
}
;To indicate an H&S problem on the site we change the text colour of the site name to Red
;We start of with the colour as Green as a default
TextColor := "Green"
logRiskAssessment = 0
If DRARed
{
	;If a DRA risk has been identified we append the risk to the maintenance comments
	logMaintenanceComment .= "Health and Safety Issue: " . DRAReason . "| "
	logRiskAssessment = 1
	;And change the text colour to red
	TextColor := "Red"
}
;Same for the PSRA
If PSRARed
{
	;If a PSRA risk has been identified we append the risk to the maintenance comments
	logMaintenanceComment .= "Public Health and Safety Issue: " . PSRAReason . "| "
	TextColor := "Red"
	logRiskAssessment = 1
}

;==========================================================================
;==========================================================================
;We build the main data collection UI
;==========================================================================
;==========================================================================
;
; There are three main tabs. 
; The first and third are common to all site types and hold information such as DateTime, Officer name and information about the maintenance of the site
; What is displayed on the second tab is dependant on the site type.
; The main data collection fields are hidden behind various buttons as this allows for more to be crammed in to the limited space available on the screen
; It does mean that users have to navigate multiple screens but that is the compromise that has to be made
 
Gui, 10: -Caption +Border
Gui, 10: Add, Text, w220 x10 y10 Center c%TextColor%, % siteName
Gui, 10: Add, Tab2, w220 h230 x10 y30 vDataTabs, Details|Data|Maint

;As stated above, these two tabs are static so we can safely add the same fields to each
Gui, 10: Tab, Details
Gui, 10: Add, Text, w200 x20 y60 cBlue Center, Date/Time
Gui, 10: Add, Edit, w140 x20 y80 vlogDateTime ReadOnly, % CurrentTime()
Gui, 10: Add, Button, w50 h20 y80 x170 gCurrentTime, Update
Gui, 10: Add, Text, w70 x20 y120 Right, Site Number:
Gui, 10: Add, Text, w90 x105 y120 Left, % siteID
Gui, 10: Add, Text, w70 x20 y155 Right, NGR:
Gui, 10: Add, Text, w90 x105 y155 Left, % NGR
Gui, 10: Add, Text, w70 x20 y190 Right, Round Pos:
Gui, 10: Add, Text, w90 x105 y190 Left, % roundPosition
Gui, 10: Add, Text, w70 x20 y225 Right, Officer:
Gui, 10: Add, Edit, w110 x105 y222 R1 vlogOfficer -Wrap, % logOfficer

Gui, 10: Tab, Maint
Gui, 10: Add, Text, w100 x20 y60 Center, Maint Issues?
Gui, 10: Add, DDL, w90 x130 y57 vlogMaintenanceIssues, Yes|No
Gui, 10: Add, Text, w100 x20 y85 Center, Date of last work:
Gui, 10: Add, DateTime, w90 x130 y82 vlogDateLastMaintenance, dd/MM/yyyy
Gui, 10: Add, Text, w200 x20 y110 Center cBlue, Maintenance Comments
Gui, 10: Add, Edit, w200 x20 y125 h80 vlogMaintenanceComment, % logMaintenanceComment
Gui, 10: Add, Button, w200 x20 y220 h30, Outstation Status

;The data tab needs to be individualised for each site type so we use GoSub to get the correct code for each

Gui, 10: Tab, Data
If InStr(siteType, "TBR")
{
	Gui, 10: Add, Button, w200 x20 y60 h50 gTBRDataEntry vTBRButton, RainGauge Data
}
Else
{
	Gui, 10: Add, Button, w90 x20 y60 h50 gReferenceDataEntry vReferenceButton, Ref. Data
	Gui, 10: Add, Button, w90 x130 y60 h50 gDataParameterEntry vDataButton Disabled, Station Data ;this button is disabled until there is at least one reference level 
}
Gui, 10: Add, Text, w200 y120 x20 Center cBlue, Site Visit Comments
Gui, 10: Add, Edit, w200 h100 y140 x20 vlogComments,

Gui, 10: Tab
Gui, 10: Add, Button, w60 x10 y270 h20, Close
Gui, 10: Add, Button, w60 x170 y270 h20, Save
Gui, 10: Show, x0 y23 w240 h300, % siteType
SIPControl(1)
Return


;#####################################################################################################################
;#####################################################################################################################
;The close button on Gui 10 cancels the data collection and resets the original Gui

10ButtonClose:
MsgBox, 20, FieldLogs Lite, You will loose all the data that you have collected.`nDo you want to continue?
IfMsgBox, No
{
	Return
}
Gui, 10: Destroy
Gui, 1: Default
Gui, 1: Show
SIPControl(1)
ClearGui()
ClearAllVars()
Return


;#####################################################################################################################
;#####################################################################################################################
;The save button will create the specific xml file based on the data entered and the site type

10ButtonSave:
Gui, 10: Submit, NoHide
;==========================================
; This code was originally intended to catch any empty variables and display them to the user.
; This was to try and catch any variables which had been added via the PARAM editing option.
; In practise it was ugly and annoying as it displayed a UI for each empty variable (could be quite an extensive list)
; 
; It has been superceded by the ability to add params which show up in the general parameter list in the paramdata.ahk file
; parameters can be added to siteType lists and excluded using the notlistedparams list.
;
; I leave this legacy code here in case there is a call for it in the future.
/*
;First we check for missed data. This will catch any variables which have been added through the param settings
;the site type name that is saved in the Ini file is slightly different to the stored variable siteType so we need to reformat it
StringLower, InisiteType, siteType
StringReplace, InisiteType, InisiteType, %A_Space%, , A
StringReplace, InisiteType, InisiteType, -, , A
;get the list of parameters from the ini file
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, % InisiteType
MissedVars := ""
;parse through the params and make a note of any which don't have a value
Loop, Parse, params, |
{
	If !A_LoopField or (A_LoopField = "ERROR")
	{
		Continue
	}
	If !(%A_LoopField%)
	{
		MissedVars .= A_LoopField . "|"
	}
}
;If we find that there are variables which don;t have a value we ask the user if they want to add data to them
If MissedVars
{
	MsgBox, 36, FieldLogs Lite, Some data fields haven't been filled.`nWould you like to add data to them? ;'
	;If they do we show them a new window using the ShowMissedVar() function. 
	IfMsgBox, Yes
	{
		;We allow the user to cancel out of this loop we have to check a variable. first we set it to blank
		MissedVarCancel =
		;we then loop through the missed variables
		Loop, Parse, MissedVars, |
		{
			;This is a holding loop. It checks for the existence of the window collecting data for a given variable and stops this loop from progresssing while it is open
			While, WinExist("No Data")
			{
				Continue
			}
			;As soon as the window is closed the script advances to here
			;The window was either closed because data was entered and saved or because the process was cancelled
			If MissedVarCancel
			{
				;If the process was cancelled, we break out of this loop
				Break
			}
			;If the process wasn't cancelled we show the next empty variable in the list
			ShowMissedVar(A_LoopField)
		}
		;After the loop we make sure that the missedVars UI is closed
		Gui, 8: Destroy
		;If the user got to the end of the process by viewing each missed variable we show a message to say that the process is ended
		if !MissedVarCancel
		{
			MsgBox, 64, FieldLogs Lite, All data fields have been viewed, 5
		}
		;Otherwise we just clear up ready for next time
		Else
		{
			MissedVarCancel = 0
		}
	}
}
*/

;First check that the Save button wasn't hit by accident
MsgBox, 36, FieldLogs Lite, You are about to save the collected data.`n`nWould you like to continue?
IfMsgBox, No
{
	Return
}

;Ask if the user wants to change the set of reasons for the visit
MsgBox, 36, FieldLogs Lite, Would you like to edit the reasons for this site visit?
IfMsgBox, Yes
{
	SaveDetails = 1
	GoSub, ButtonOK
	While (SaveDetails)
	{
		Continue
	}
}

SaveDetails:

;We ask if we should set the visit as rective. This changes the roundName to REACTIVE VISIT
MsgBox, 36, FieldLogs Lite, Would you like to set this visit as Reactive?
IfMsgBox, Yes
{
	prevRoundName := roundName
	roundName := "REACTIVE VISIT"
}
;We set the KPI tag to No as the data won't have been processed in WISKI
logKPI := "No"
;Based on the site type we send the necessary data fields to the function which builds the xml file
If (siteType = "Flow Structures") or (siteType = "Flow Rated Section")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, flowstructures,
	BuildXML("ns5", "Structure", params)
}
If (siteType = "Flow Ultrasonic")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, flowultrasonic,
	BuildXML("ns2", "Ultrasonic", params)
}
If (siteType = "Flow Electromagnetic")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, flowelectromagnetic,
	BuildXML("ns4", "Electromagnetic", params)
}
If (siteType = "Flow Velocity-Index")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, flowvelocityindex,
	BuildXML("ns8", "VelocityIndex", params)
}
If (siteType = "Flow Hybrid")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, flowhybrid,
	BuildXML("ns9", "Hybrid", params)
}
If (siteType = "Level") or (siteType = "Level Control Structure")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, level,
	BuildXML("ns3", "Level", params)
}
If (siteType = "Rainfall TBR")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, tbr,
	BuildXML("ns6", "TBR", params)
}
If (siteType = "Groundwater")
{
	IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, groundwater,
	BuildXML("ns7", "Groundwater", params)
}
Gui, 10: Destroy
Gui, 1: Default
Gui, 1: Show
If prevRoundName
{
	roundName := prevRoundName
}
ClearGui()
ClearAllVars()
Return


;The Outstation Status button sits at the bottom of the Maint tab and allows for battery voltages etc. to be collected
10ButtonOutstationStatus:
Gui, 11: -Caption +Border
Gui, 11: Add, Text, w220 x10 y15 Center cBlue, Outstation Status
Gui, 11: Add, Text, w120 x10 y40 Center, OS Time Adjusted?
Gui, 11: Add, DDL, w90 x140 y37 vOS_Time, Yes|No
GuiControl, 11: ChooseString, OS_Time, % OS_Time
Gui, 11: Add, Text, w120 x10 y70 Center, DC Volts:
Gui, 11: Add, Edit, w90 x140 y67 vOS_External_Voltage gCheck_OS_External_Voltage, % OS_External_Voltage
Gui, 11: Add, Text, w120 x10 y95 Center, Internal Volts:
Gui, 11: Add, Edit, w90 x140 y92 vOS_Internal_Voltage gCheck_OS_Internal_Voltage, % OS_Internal_Voltage
Gui, 11: Add, Text, w120 x10 y120 Center, Battery Changed?
Gui, 11: Add, DDL, w90 x140 y117 vOS_Battery_Changed, Yes|No
GuiControl, 11: ChooseString, OS_Battery_Changed, % OS_Battery_Changed
Gui, 11: Font, s7
Gui, 11: Add, Text, w120 x10 y155 Center, Logger Time Adjusted?
Gui, 11: Font, s9
Gui, 11: Add, DDL, w90 x140 y152 vLogger_Time, Yes|No
GuiControl, 11: ChooseString, Logger_Time, % Logger_Time
Gui, 11: Add, Text, w120 x10 y180 Center, Logger Volts:
Gui, 11: Add, Edit, w90 x140 y177 vLogger_Battery_Voltage gCheck_Logger_Battery_Voltage, % Logger_Battery_Voltage
Gui, 11: Add, Text, w120 x10 y205 Center, Battery Changed?
Gui, 11: Add, DDL, w90 x140 y202 vLogger_Battery_Changed, Yes|No
GuiControl, 11: ChooseString, Logger_Battery_Changed, % Logger_Battery_Changed
Gui, 11: Add, Text, w120 x10 y235 Center, Meter Reading:
Gui, 11: Add, Edit, w90 x140 y232 vMeter_Reading, % Meter_Reading
Gui, 11: Add, Button, w60 x10 y270 h20, Back
Gui, 11: Add, Button, w60 x170 y270 h20, Save
Gui, 11: Show, x0 y23 w240 h300, Outstation Status
SIPControl(1,1)
Return

11ButtonSave:
Gui, 11: Submit
11ButtonBack:
Gui, 11: Destroy
Gui, 10: Default
Gui, 10: Show
SIPControl(1)
Return
