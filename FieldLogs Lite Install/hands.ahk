﻿;#####################################################################################################################
;#####################################################################################################################
; This is the start of Site data collection
;#####################################################################################################################
;#####################################################################################################################
;when the Site Directions button is pressed we show them to the user in a new window
;as the directions can be multilined we give them their own window 

ButtonSiteDirections:
Gui, 2: -Caption +Border
Gui, 2: Add, Edit, w220 h250 x10 y10 vSiteDirections ReadOnly,
GuiControl, 2: , SiteDirections, % SiteDirections
Gui, 2: Add, Button, w60 x170 y270 h20, Close
Gui, 2: Show, x0 y23 w240 h300, Site Directions - %siteID%
SIPControl(0)
Return

2ButtonClose:
Gui, 2: Destroy
Gui, 1: Default
Gui, 1: Show
Return

;#####################################################################################################################
;when the Site Comments button is pressed we show them to the user ina new window

ButtonSiteComments:
Gui, 3: -Caption +Border
Gui, 3: Add, Edit, w220 h250 x10 y10 vTAR_SiteComments ReadOnly,
GuiControl, 3: , TAR_SiteComments, % TAR_SiteComments
Gui, 3: Add, Button, w60 x170 y270 h20, Close
Gui, 3: Show, x0 y23 w240 h300, Site Comments - %siteID%
SIPControl(0)
Return

3ButtonClose:
Gui, 3: Destroy
Gui, 1: Default
Gui, 1: Show
Return


;#####################################################################################################################
;#####################################################################################################################
;The user has to green card the site on both DRA and PSRA before collecting data. 
;If they red card the site, we want to know why

;This subroutine is run every time a selection is made, red or green, on the DRA section
DRA:
Gui, Submit, NoHide
;If the user hit red we show a new window where they can enter their reasons for doing so
If DRARed
{
	Gui, 4: -Caption +Border
	Gui, 4: Add, Text, w220 x10 y10 R3, Remember that you MUST phone your team leader to let them know that you have RED CARDED site %SiteID%!
	Gui, 4: Add, Text, w220 x10 y60 R3, You must also make a note below of the problems on site so that they can be sorted out.
	Gui, 4: Add, Edit, w220 x10 y110 h140 vDRAReason, % DRAReason
	Gui, 4: Add, Button, w60 x10 h20 y270, Back
	Gui, 4: Add, Button, w60 x170 h20 y270, Save
	Gui, 4: Show, x0 y23 w240 h300, DRA Red Card - %siteID%
	SIPControl(1,1) ;Show SIP button and keyboard
}
;If they hit green we check the status of the PSRA green card and activate the OK button if both have a value.
;user can still collect data if site has been red carded
If (DRAGreen or DRARed) and (PSRAGreen or PSRARed)
{
	GuiControl, Enable, DataCollectOKButton
}
Return

;When the user hits save we want to check that they entered their reasons
4ButtonSave:
Gui, 4: Submit, NoHide
;If they left the box blank we warn them
If !DRAReason
{
	MsgBox, 16, FieldLogs Lite, You must note the problems on the site or press the back button.
	Gui, 4: Show
}
;Otherwise we save the reasons before returning to a blank initial form
Else
{
	;The reason is saved in memory and is appended to the Maintenance comments in the data collection area of the field logs 
	Gui, 4: Destroy
	Gui, 1: Default
	Gui, 1: Show
	SIPControl(0) ;Hide SIP button and keyboard
}
Return

;If they hit the back button we allow them to return to the initial form page.
4ButtonBack:
Gui, 4: Destroy
Gui, 1: Default
Gui, 1: Show
SIPControl(0) ;Hide SIP button and keyboard
GuiControl, , DRARed, 0
Return

;#####################################################################################################################
;This next section is the same as the DRA section but deals specifically with PSRA risks

PSRA:
Gui, Submit, NoHide
If PSRARed
{
	Gui, 5: -Caption +Border
	Gui, 5: Add, Text, w220 x10 y10 R3, Remember that you MUST phone your team leader to let them know that you have RED CARDED site %SiteID%!
	Gui, 5: Add, Text, w220 x10 y60 R3, You must also make a note below of the problems on site so that they can be sorted out.
	Gui, 5: Add, Edit, w220 x10 y110 h140 vPSRAReason, % PSRAReason
	Gui, 5: Add, Button, w60 x10 h20 y270, Back
	Gui, 5: Add, Button, w60 x170 h20 y270, Save
	Gui, 5: Show, x0 y23 w240 h300, PSRA Red Card - %siteID%
	SIPControl(1,1) ;Show SIP button and keyboard
}
If (DRAGreen or DRARed) and (PSRAGreen or PSRARed)
{
	GuiControl, Enable, DataCollectOKButton
}
Return

5ButtonSave:
Gui, 5: Submit, NoHide
If !PSRAReason
{
	MsgBox, 16, FieldLogs Lite, You must note the problems on the site or press the back button.
	Gui, 5: Show
}
Else
{
	;The reason is saved in memory and is appended to the Maintenance comments in the data collection area of the field logs 	
	Gui, 5: Destroy
	Gui, 1: Default
	Gui, 1: Show
	SIPControl(0) ;Hide SIP button and keyboard
}
Return

5ButtonBack:
Gui, 5: Destroy
Gui, 1: Default
Gui, 1: Show
SIPControl(0) ;Hide SIP button and keyboard
GuiControl, , PSRARed, 0
Return
