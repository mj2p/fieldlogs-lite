﻿;#####################################################################################################
; Functions
;#####################################################################################################


;======================================================================================================
;BuildXML creates the output xml file after the data has been collected
;We scan every required parameter for the specified site type and save the data 
;for every one which has had data collected for it
;
BuildXML(prefix, type, xmlparams) {
	;Set the function as global. bad practise but means that it definitely works 
	global
	;Get rid of the <site_number>.xml file that was exported from the database as we're about to create our own
	FileDelete, % XMLDir . "\" . siteID . ".xml"
	;If there was a problem
	If ErrorLevel
	{
		;ket the user know
		MsgBox, 16, FieldLogs Lite, An error occured when writing the data to file.`nPlease try again
		Return
	}
	;ensure the header is proper XML encoding
	XML := "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>`r`n"
	;add in the siteType specific prefixes and strings
	XML .= "<" . prefix . ":" . type . " xsi:schemaLocation=""FieldLog_" . type . " fls_" . type . ".xsd"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:" . prefix . "=""FieldLog_" . type . """>`r`n"
	;The xmlparams string should be every parameter which is required by the given siteType form the settings.ini file
	Loop, Parse, xmlparams, |
	{
		;If we have an empty parameter, or the ini read went wrong or there is a blank param
		If !A_LoopField or (%A_LoopField%="ERROR") or !(%A_LoopField%)
		{
			;move to the next one
			Continue
		}
		;we allow ther user to specify words for boolean values.
		If (%A_LoopField% = "Yes") or (%A_LoopField% = "No") or (%A_LoopField% = "Adjust?")
		{
			;If the value is yes we set boolean 1. anything else gets boolean 0
			%A_LoopField% := "Yes" ? 1 : 0
		}
		;copy the current data value to the Data variable
		Data := %A_LoopField%
		;properly escape the special characters so that the xml file is properly formatted and valid
		StringReplace, Data, Data, &, &amp;, A
		StringReplace, Data, Data, <, &lt;, A
		StringReplace, Data, Data, >, &gt;, A
		StringReplace, Data, Data, ", &quot;, A ;"
		StringReplace, Data, Data, ', &apos;, A ;'
		;and finally append the XML string conatining the current data value to the XML file as it stands
		XML .= A_Tab . "<" . prefix . ":" . A_LoopField . ">" . Data . "</" . prefix . ":" . A_LoopField . ">`r`n"
	}
	;once we've scanned all the parameters we need to terminate the XML node
	XML .= "</" . prefix . ":" . type . ">"
	;We're done. all that remainsis top write the file to disk
	FileAppend, % XML, % XMLDir . "\" . siteID . ".xml"
	;If that goes awry, we warn the user
	If ErrorLevel
	{
		MsgBox, 16, FieldLogs Lite, An error occured when writing the data to file.`nPlease try again
		Return
	}
	;otherwise tell them of the success
	MsgBox, 0, FieldLogsLite, Log for %siteName% - %siteID% has been saved.
	Return
}

;======================================================================================================
;Get_refHeirarchy returns the reference level highest in the pecking order
;When we calculate the difference between the reference data and the equipment data 
;we need to know which reference data point to use  
;this function returns whichever one has data and is highest in the 'pecking' order.
Get_refHeirarchy(location)	{
	;Return whatever value exists in this order
	If (Level_%location%_Internal)
	{
		Return % Level_%location%_Internal
	}
	Else If (Level_%location%_External)
	{
		Return % Level_%location%_External
	}
	Else If (Level_%location%_GB_Local)
	{
		Return % Level_%location%_GB_Local
	}
	Else
	{
		Return 0
	}
}

;LoadRound does just that
LoadRound() {
	global Progress_Text, Progress
	global roundName, siteNames, XMLDir,siteNamesMatchList
	;Build a list of all the sites available in the $round.xml file 
	siteNames := ""
	count := ""
	IniRead, XMLDir, \Program Files\FieldLogs Lite\settings.ini, settings, xmldir, %A_Space%
	If !FileExist(XMLDir . "\$round.xml")
	{
		MsgBox, 16, FieldLogs Lite, No $round.xml file was found.`nPlease set your xml folder.
		GoSub, ButtonSettings
		Return
	}
	;display this splash screen to indicate that something is happening.
	SIPControl(0) ;Hide SIP button and keyboard
	Gui, 99: -Caption +Border
	Gui, 99: Add, Text, x10 y60 W240 vProgress_Text,
	Gui, 99: Add, Text, x10 y75 W240 vProgress, 
	Gui, 99: Show, x0 y23 W240 h300, FieldLogs Lite Progress
	Loop, Read, % XMLDir . "\$round.xml"
	{
		If !InStr(A_LoopReadLine, "fr:Site")
		{
			If InStr(A_LoopReadLine, "<fr:Round")
			{
				roundName := StrX(A_LoopReadLine, "roundName", 1, 11, "exportDate", True, 12, N)
				GuiControl, 99: , Progress_Text, Loading Sites for %roundName%
			}
			Continue
		}
		count += 1
		GuiControl, 99: , Progress, % count
		;We read through the file line by line and extract the site name from the xml
		;In some cases there isn;t a visit frequency listed for the site so we deal with that eventuality first
		If !InStr(A_LoopReadLine, "currentVisitFrequency")
		{
			siteNames .= StrX(A_LoopReadLine, "siteName", 1, 10, """>", True, 2, N) . "|"
		}
		Else
		{
			siteNames .= StrX(A_LoopReadLine, "siteName", 1, 10, "currentVisitFrequency", True, 23, N) . "|"
		}
	}
	StringTrimRight, siteNames, siteNames, 1
	Sort, siteNames, D|
	;So that only true site names are found we need a match list of site names
	StringReplace, siteNamesMatchList, SiteNames, |, `,, A
	Gui, 1: Default
	ClearGui()
	Gui, 1: Show
	Gui, 99: Destroy
	SIPControl(1)
}

;ClearGui resets the main Gui ready for a new site to be selected
ClearGui() {
	global siteNames
	global roundName
	GuiControl, , DRARed, 0
	GuiControl, , DRAGreen, 0
	GuiControl, , PSRARed, 0
	GuiControl, , PSRAGreen, 0
	GuiControl, , siteID, 
	GuiControl, , siteType,
	GuiControl, , NGR, 
	GuiControl, , datum, 
	GuiControl, ChooseString, MainTabs, Details
	GuiControl, , siteName, |%siteNames%
	GuiControl, , Round, % "Round: " . roundName
	ClearAllVars()
	Return
}

;StrX is the function we use to parse the xml files
StrX( H,  BS="",BO=0,BT=1,   ES="",EO=0,ET=1,  ByRef N="" ) { ;    | by Skan | 19-Nov-2009
	Return SubStr(H,P:=(((Z:=StrLen(ES))+(X:=StrLen(H))+StrLen(BS)-Z-X)?((T:=InStr(H,BS,0,((BO
 <0)?(1):(BO))))?(T+BT):(X+1)):(1)),(N:=P+((Z)?((T:=InStr(H,ES,0,((EO)?(P+1):(0))))?(T-P+Z
 +(0-ET)):(X+P)):(X)))-P) ; v1.0-196c 21-Nov-2009 www.autohotkey.com/forum/topic51354.html
}

;CurrentTime formats the current time string to make it human readable
CurrentTime() {
	FormatTime, CurrentTime, %A_NowUTC%, dd/MM/yyyy HH:mm
	Return CurrentTime
	
	CurrentTime:
	GuiControl, 10: , DateTime, % CurrentTime()
	Return
}

;TextAlert turns a controls text red if it is greater than a given amount
TextAlert(control, amount, gui) {
	global
	Gui, %gui%: Submit, NoHide 
	If (sqrt(%control% * %control%) > amount)
	{
		Gui, %gui%: Font, cRed
	}
	Else
	{
		Gui, %gui%: Font, cGreen	
	}
	GuiControl, %gui%: Font, % control
	Return
	
	Check_OS_External_Voltage:
	IniRead, DCVoltsAmount, \Program Files\FieldLogs Lite\settings.ini, values, dcvolts, 30
	TextAlert("OS_External_Voltage", DCVoltsAmount, 11)
	Return
	
	Check_OS_Internal_Voltage:
	IniRead, InternalVoltsAmount, \Program Files\FieldLogs Lite\settings.ini, values, internalvolts, 30
	TextAlert("OS_Internal_Voltage", InternalVoltsAmount, 11)
	Return
	
	Check_Logger_Battery_Voltage:
	IniRead, LoggerVoltsAmount, \Program Files\FieldLogs Lite\settings.ini, values, loggervoltsolts, 12
	TextAlert("Logger_Battery_Voltage", LoggerVoltsAmount, 11)
	Return
}

;Get data from the Gui and calculate the difference. also highlight text value errors.
CalcLevel(master, subtract, destination, gui, amount=0) {
	global
	Gui, %gui%: Submit, NoHide
	If !(%subtract%)
	{
		GuiControl, %gui%: , % destination,
		Return
	}
	Else
	{
		GuiControl, %gui%: , % destination, % Round((%master% - %subtract%), 3)
	}
	
	If amount
	{
		%destination% := sqrt(%destination% * %destination%)
		TextAlert(destination, amount, gui)
	}
	Return % Round((%master% - %subtract%), 3)
}

;SIPControl controls whether the soft input button and /or the keyboard is shown or hidden
SIPControl(ShowButton=0,ShowKeyboard=0) {
	If ShowButton
	{
		WinShow, MS_SIPBUTTON
		If ShowKeyboard
		{
			DllCall("SipShowIM", "UInt", 1) ;Show
		}
		Else
		{
			DllCall("SipShowIM", "UInt", 0) ;Hide
		}
	}
	Else
	{
		DllCall("SipShowIM", "UInt", 0) ;Hide
		WinHide, MS_SIPBUTTON
	}
	Return
}

;Clear all variables ready to collect data again
ClearAllVars() {
	global
	IniRead, flowstructure, \Program Files\FieldLogs Lite\settings.ini, params, flowstructure,
	IniRead, flowultrasonic, \Program Files\FieldLogs Lite\settings.ini, params, flowultrasonic,
	IniRead, flowelectromagnetic, \Program Files\FieldLogs Lite\settings.ini, params, flowelectromagnetic,
	IniRead, flowvelocityindex, \Program Files\FieldLogs Lite\settings.ini, params, flowvelocityindex,
	IniRead, flowhybrid, \Program Files\FieldLogs Lite\settings.ini, params, flowhybrid,
	IniRead, level, \Program Files\FieldLogs Lite\settings.ini, params, level,
	IniRead, tbr, \Program Files\FieldLogs Lite\settings.ini, params, tbr,
	IniRead, groudwater, \Program Files\FieldLogs Lite\settings.ini, params, groundwater,
	params := flowstructure . "|" . flowultrasonic . "|" . flowelectromagnetic . "|" . flowvelocityindex . "|" . flowhybrid . "|" . level . "|" . tbr . "|" . groundwater
	Loop, Parse, params, |
	{
		If !A_LoopField or (A_LoopField = "ERROR")
		{
			Continue
		}
		If (A_LoopField = "roundName")
		{
			Continue
		}
		VarSetCapacity(%A_LoopField%, 0)
	}
	Return
}

;ShowMissedVar will display a simple Gui to allow the addition of extra variables if needed

ShowMissedVar(var) {
	global
	Gui, 8: -Caption +Border
	Gui, 8: Add, Text, w220 x10 y40 cBlue, Empty Data Field
	StringReplace, humanVar, var, _, %A_Space%, A
	StringUpper, humanVar, humanVar, T
	Gui, 8: Add, Text, w120 x10 y80, %humanVar%
	Gui, 8: Add, Edit, w220 x10 y100 v%var%,
	Gui, 8: Add, Button, w100 x130 h30 y140, Cancel
	Gui, 8: Add, Button, w60 x10 y270 h20, Ignore
	Gui, 8: Add, Button, w60 x170 y270 h20, Save
	Gui, 8: Show, x0 y23 w240 h300, No Data
	SIPControl(1, 1)
	Return
	
	8ButtonIgnore:
	Gui, 8: Destroy
	Return
	
	8ButtonSave:
	Gui, 8: Submit
	Gui, 8: Destroy
	Return
	
	8ButtonCancel:
	MsgBox, 36, FieldLogs Lite, Are you sure you want to cancel?`nSome data fields still don't have data ;'
	IfMsgBox, Yes
	{	
		MissedVarCancel = 1
		Gui, 8: Destroy
	}
	Else
	{
		Gui, 8: Show
	}
	Return
}


;################################################################################################################
;################################################################################################################
; Ini Functions
;################################################################################################################

Ini_Load(ByRef _Content, _Path = "", _convertNewLine = false) {
	Ini_BuildPath(_Path)
    error := true ; If file is found next, then its set to false.
    Loop, %_Path%, 0, 0
    {
        _Path := A_LoopFileLongPath
        error := false
        Break
    }    
    If (error = false)
    {
		FileRead, _Content, %_Path%
		If ErrorLevel
        {
            error := true
        }
    }
    If (error)
    {
        _Content := ""
    }
    Else If (_convertNewLine)
    {
        StringReplace, _Content, _Content, `r`n, `n, All
    }
    ErrorLevel := error
    Return _Path
}

Ini_BuildPath(ByRef _path) {
    ; Set to default wildcard if filename or exension are not set.
    If (_Path = "")
    {
        _Path := RegExReplace(A_ScriptFullPath, "S)\..*?$") . ".ini"
    }
    Else If (SubStr(_Path, 0, 1) = "\")
    {
        _Path .= RegExReplace(A_ScriptName, "S)\..*?$") . ".ini"
    }
    Else
    {
        If (InStr(FileExist(_Path), "D"))
        {
            ; If the current path is a directory, then add default file pattern to the directory.
            _Path .= "\" . RegExReplace(A_ScriptName, "S)\..*?$") . ".ini"
        }
        Else
        {
            ; Check all parts of path and use defaults, if any part is not specified.
            SplitPath, _Path,, fileDir, fileExtension, fileNameNoExt
            If (fileDir = "")
            {
                fileDir := A_WorkingDir
            }
            If (fileExtension = "")
            {
                fileExtension := "ini"
            }
            If (fileNameNoExt = "")
            {
                fileNameNoExt := RegExReplace(A_ScriptName, "S)\..*?$")
            }
            _Path := fileDir . "\" . fileNameNoExt . "." . fileExtension
        }
    }
    Return 0
}

Ini_GetSection(ByRef _Content, _Section) {
    If (_Section = "")
        _Section = (?:\[.*])?
    Else
         _Section = \[\s*?\Q%_Section%\E\s*?]
    RegEx = `aisUS)^.*(%_Section%\s*\R?.*)(?:\R*\s*(?:\[.*?|\R))?$
    If RegExMatch(_Content, RegEx, Value)
        ErrorLevel = 0
    Else
    {
        ErrorLevel = 1
        Value1 =
    }
    Return Value1
}

Ini_GetAllKeyNames(ByRef _Content, _Section = "", ByRef _count = "") {
    RegEx = `aisUmS)^.*(?:\s*\[\s*?.*\s*?]\s*|\s*?(.+)\s*?=.*).*$
    If (_Section != "")
        KeyNames := RegExReplace(ini_getSection(_Content, _Section), RegEx, "$1", Match)
    Else
        KeyNames := RegExReplace(_Content, RegEx, "$1", Match)
    If Match
    {
        KeyNames := RegExReplace(KeyNames, "S)\R+", ",")
        ; Workaround, sometimes it catches sections. Whitespaces only should be eliminated also.
        KeyNames := RegExReplace(KeyNames, "S)\[.*?],+|,+$|,+ +", "") 
        StringReplace, KeyNames, KeyNames, `,, `,, UseErrorLevel
        _count := ErrorLevel ? ErrorLevel : 0
        StringTrimLeft, KeyNames, KeyNames, 1
        ErrorLevel = 0
    }
    Else
    {
        ErrorLevel = 1
        _count = 0
        KeyNames =
    }
    Return KeyNames
}
