﻿;#####################################################################################################################
;Historic Data subroutines here
;
;The historic data is held in INI files which are produced by the Desktop Widget
;There is one file for each site with the last three visits in each numbered sections 1,2 and 3
;All the data for those three visits that is available in the recent logs.mdb database produced by the Fie;ld Logs system is available here

;The comments are hidden behind a button as they may be multi lined and therefore require a full screen eidt box
HistoricComments:
If FileExist("\My Documents\FieldLogs Lite\Historic\" . SiteID . ".ini")
{
	;The button has the number 1,2 or 3 in the name so we can use the name of the button which launched this thread to determine which visits comments we need to load
	StringReplace, visit, A_GuiControl, Visit, , A
	StringReplace, visit, visit, Comments, , A
	;Once we have the visit number we can get the data. We get the officer and datatime to display at the top so that the user is remoinded what they are looking at
	IniRead, logOfficer, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logOfficer, %A_Space%
	IniRead, logDateTime,  \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logDateTime, %A_Space%
	IniRead, logComments, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logComments, %A_Space%
	IniRead, logMaintenanceComment, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logMaintenanceComment, %A_Space%

	;Build a full page GUI to display the data we jsut parsed
	Gui, 20: Destroy
	Gui, 20: -Caption +Border
	Gui, 20: Add, Text, w220 y10 x10 Center, %logOfficer% - %logDateTime%
	Gui, 20: Add, Text, w220 y30 x10 Center, logComments
	Gui, 20: Add, Edit, w220 y50 x10 R6 ReadOnly, %logComments%
	Gui, 20: Add, Text, w220 y150 x10 Center, logMaintenacecomment
	Gui, 20: Add, Edit, w220 y170 x10 R6 ReadOnly, %logMaintenanceComment%
	
	Gui, 20: Add, Button, w60 x170 y270 h20, Close
	Gui, 20: Show, x0 y23 w240 h300, FieldLogs Lite
	SIPControl(0)
}
Return

;The Data is also held behind a button
HistoricData:
If FileExist("\My Documents\FieldLogs Lite\Historic\" . SiteID . ".ini")
{
	;again we determine which visit has been requested
	StringReplace, visit, A_GuiControl, Visit, , A
	StringReplace, visit, visit, Data, , A
	;And get the data
	IniRead, %visit%Officer, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logOfficer, None
	IniRead, %visit%DateTime, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, logDateTime, None
	
	;These functions allow us to load all the INi keys without knowing them before hand. This is useful if the data format ever changes in the future as this will still extract the data
	Ini_Load(logData, "\My Documents\FieldLogs Lite\Historic\" . SiteID . ".ini")
	Keys := Ini_GetAllKeyNames(logData, visit)	
	
	;This GUI conatins a listview which simply lists the parameter with it's associasted value.
	;not the prettiest to look at but ensures future compatability
	Gui, 20: Destroy
	Gui, 20: -Caption +Border
	Gui, 20: Add, Text, w220 y10 x10 Center, % %visit%Officer . "-" . %visit%DateTime
	Gui, 20: Add, ListView, Grid w220 y30 x10 h170 gHistoricDisplayParam AltSubmit vHistoricParams, Parameter|Value
	Gui, 20: Default
	LV_ModifyCol(1, 130)
	LV_ModifyCol(2, 79)
	;By parsing the keys we can extract each value and add the pair to the ListView
	Loop, Parse, Keys, `,
	{
		IniRead, value, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %visit%, %A_LoopField%, %A_Space%
		LV_Add("", A_LoopField, value)
	}	
	Gui, 20: Add, Text, w220 x10 y210 cBlue Center vhistoricParam,
	Gui, 20: Add, Edit, w220 x10 y230 vhistoricParamData R2, 
	Gui, 20: Add, Button, w60 x170 y270 h20, Close
	Gui, 20: Show, x0 y23 w240 h300, FieldLogs Lite
	SIPControl(0)
}
Return


;Closing either Historic data UI goes back to the main window
20ButtonClose:
Gui, 20: Destroy
Gui, 1: Default
Gui, 1: Show
Return

HistoricDisplayParam:
Gui, 20: Default
Gui, 20: ListView, HistoricParams
If (A_GuiEvent = "Normal")
{
	LV_GetText(historicParam, A_EventInfo, 1)
	LV_GetText(historicParamData, A_EventInfo, 2)
	If ((historicParam = "Param") || (historicParamData = "Value"))
	{
		Return
	}
	GuiControl, 20: , historicParam, % historicParam
	GuiControl, 20: , historicParamData, % historicParamData
}
Return