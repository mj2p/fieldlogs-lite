﻿;#####################################################################################################################
ReferenceDataEntry:
; Here we allow the user to enter reference data. 
; This separates out the reference data and the Outstaion data
; We build the UI dynamically based on the params available for the SiteType

Gui, 13: -Caption +Border
;Get the SiteType in the correct format for the settings.ini file
StringLower, iniType, SiteType
StringReplace, iniType, iniType, %A_Space%, , A
StringReplace, iniType, iniType, -, , A
;Get the list of SiteType specific parameters
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, %iniType%, %A_Space%

;Groundwater is a special case as it has unique parameters
If (SiteType = "Groundwater")
{
	Gui, 13: Add, Text, w220 x10 y20 cBlue Center, Groundwater Levels
	Gui, 13: Add, Text, w90 x10 y60 Center, Datum
	Gui, 13: Add, Edit, w110 x110 y57 ReadOnly vdatum, %datum%
	Gui, 13: Add, Text, w90 x10 y85 Center, Datum Offset
	Gui, 13: Add, Edit, w110 x110 y82 ReadOnly vgroundwater_Datum_Offset, %groundwater_Datum_Offset%
	Gui, 13: Add, Text, w90 x10 y110 Center, Dip Reading
	Gui, 13: Add, Edit, w110 x110 y107 vDip_Groundwater_Level gCalc_Level_GW, %Dip_Groundwater_Level%
	IniRead, groundWaterMeasure, \Program Files\FieldLogs Lite\settings.ini, ossettings, groundWaterMeasure
	If (groundWaterMeasure = "mAOD")
	{
		Gui, 13: Add, Text, w90 x10 y160 Center, Level mAOD
		Gui, 13: Add, Edit, w110 x110 y157 vLevel_GW, %Level_GW%
	}
	If (groundWaterMeasure = "mBDAT")
	{
		Gui, 13: Add, Text, w90 x10 y190 Center, Level mBDAT
		Gui, 13: Add, Edit, w110 x110 y187 vLevel_GW_mBDAT, %Dip_Groundwater_Level%
	}
}
;Level and Flow sites have the same reference parameters so we can lump them all together 
Else
{
	;Split the parameters into three for Head Tail and Crest
	;The parameters that are available to fill in are dependant on the datums available and the params for the siteType
	Gui, 13: Add, Tab2, w220 h250 x10 y10 vRefLevelTabs gCheckDatums, Head|Tail|Crest|Lvl 3|Lvl 4
	
	Gui, 13: Tab, Head
	Gui, 13: Font, s8
	If (head_Internal_dipPlate_Datum || head_External_dipPlate_Datum)
	{
		Gui, 13: Add, Text, w60 x20 y70, Dip Datum
		Gui, 13: Add, Text, w60 x20 y100, Dip Reading
		Gui, 13: Add, Text, w60 x20 y130, Water Level
	}
	Gui, 13: Font, s9
	If head_Internal_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x90 y40 cBlue, Internal
		Gui, 13: Add, Edit, w60 x90 y67 vhead_Internal_dipPlate_Datum ReadOnly, %head_Internal_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x90 y97 vDip_Head_Internal gCalc_Level_Head_Internal, %Dip_Head_Internal%
		Gui, 13: Add, Edit, w60 x90 y127 vLevel_Head_Internal ReadOnly, %Level_Head_Internal%
	}
	If head_External_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x160 y40 cBlue, External
		Gui, 13: Add, Edit, w60 x160 y67 vhead_External_dipPlate_Datum ReadOnly, %head_External_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x160 y97 vDip_Head_External gCalc_Level_Head_External, %Dip_Head_External%
		Gui, 13: Add, Edit, w60 x160 y127 vLevel_Head_External ReadOnly, %Level_Head_External%
	}
	If InStr(params, "Level_Head_GB_Local") or InStr(params, "Level_Head_GB_mAOD")
	{
		Gui, 13: Font, s8
		Gui, 13: Add, Text, w60 x20 y200, Staff Gauge
		Gui, 13: Font, s9
	}
	If InStr(params, "Level_Head_GB_Local")
	{
		Gui, 13: Add, Text, w60 x90 y170 cBlue, Local
		Gui, 13: Add, Edit, w60 x90 y197 vLevel_Head_GB_Local, %Level_Head_GB_Local%
	}
	If InStr(params, "Level_Head_GB_mAOD")
	{
		Gui, 13: Add, Text, w60 x160 y170 cBlue, mAOD
		Gui, 13: Add, Edit, w60 x160 y197 vLevel_Head_GB_mAOD, %Level_Head_GB_mAOD%
	}
	
	Gui, 13: Tab, Tail
	Gui, 13: Font, s8
	If (tail_Internal_dipPlate_Datum || tail_External_dipPlate_Datum)
	{
		Gui, 13: Add, Text, w60 x20 y70, Dip Datum
		Gui, 13: Add, Text, w60 x20 y100, Dip Reading
		Gui, 13: Add, Text, w60 x20 y130, Water Level
	}
	Gui, 13: Font, s9
	If tail_Internal_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x90 y40 cBlue, Internal
		Gui, 13: Add, Edit, w60 x90 y67 vtail_Internal_dipPlate_Datum ReadOnly, %tail_Internal_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x90 y97 vDip_Tail_Internal gCalc_Level_Tail_Internal, %Dip_Tail_Internal%
		Gui, 13: Add, Edit, w60 x90 y127 vLevel_Tail_Internal ReadOnly, %Level_Tail_Internal%
	}
	If tail_External_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x160 y40 cBlue, External
		Gui, 13: Add, Edit, w60 x160 y67 vtail_External_dipPlate_Datum ReadOnly, %tail_External_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x160 y97 vDip_Tail_External gCalc_Level_Tail_External, %Dip_Tail_External%
		Gui, 13: Add, Edit, w60 x160 y127 vLevel_Tail_External ReadOnly, %Level_Tail_External%
	}
	If InStr(params, "Level_Tail_GB_Local") or InStr(params, "Level_Tail_GB_mAOD")
	{
		Gui, 13: Font, s8
		Gui, 13: Add, Text, w60 x20 y200, Staff Gauge
		Gui, 13: Font, s9
	}
	If InStr(params, "Level_Tail_GB_Local")
	{
		Gui, 13: Add, Text, w60 x90 y170 cBlue, Local
		Gui, 13: Add, Edit, w60 x90 y197 vLevel_Tail_GB_Local, %Level_Tail_GB_Local%
	}
	If InStr(params, "Level_Tail_GB_mAOD")
	{
		Gui, 13: Add, Text, w60 x160 y170 cBlue, mAOD
		Gui, 13: Add, Edit, w60 x160 y197 vLevel_Tail_GB_mAOD, %Level_Tail_GB_mAOD%
	}
	
	Gui, 13: Tab, Crest
	Gui, 13: Font, s8
	Gui, 13: Add, Text, w60 x20 y70, Dip Datum
	Gui, 13: Add, Text, w60 x20 y100, Dip Reading
	Gui, 13: Add, Text, w60 x20 y130, Water Level
	Gui, 13: Font, s9
	Gui, 13: Add, Edit, w60 x90 y67 vcrest_dipPlate_Datum ReadOnly, %crest_dipPlate_Datum%
	Gui, 13: Add, Edit, w60 x90 y97 vDip_Crest gCalc_Level_Crest, %Dip_Crest%
	Gui, 13: Add, Edit, w60 x90 y127 vLevel_Crest ReadOnly, %Level_Crest%
		
	Gui, 13: Tab, Lvl 3
	Gui, 13: Font, s8
	If (Lvl_3_Internal_dipPlate_Datum || Lvl_3_External_dipPlate_Datum)
	{
		Gui, 13: Add, Text, w60 x20 y70, Dip Datum
		Gui, 13: Add, Text, w60 x20 y100, Dip Reading
		Gui, 13: Add, Text, w60 x20 y130, Water Level
	}
	Gui, 13: Font, s9
	If Lvl_3_Internal_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x90 y40 cBlue, Internal
		Gui, 13: Add, Edit, w60 x90 y67 vLvl_3_Internal_dipPlate_Datum ReadOnly, %Lvl_3_Internal_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x90 y97 vDip_3_Internal gCalc_Level_3_Internal, %Dip_3_Internal%
		Gui, 13: Add, Edit, w60 x90 y127 vLevel_3_Internal ReadOnly, %Level_3_Internal%
	}
	If Lvl_3_External_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x160 y40 cBlue, External
		Gui, 13: Add, Edit, w60 x160 y67 vLvl_3_External_dipPlate_Datum ReadOnly, %Lvl_3_External_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x160 y97 vDip_3_External gCalc_Level_3_External, %Dip_3_External%
		Gui, 13: Add, Edit, w60 x160 y127 vLevel_3_External ReadOnly, %Level_3_External%
	}
	If InStr(params, "Level_3_GB_Local") or InStr(params, "Level_3_GB_mAOD")
	{
		Gui, 13: Font, s8
		Gui, 13: Add, Text, w60 x20 y200, Staff Gauge
		Gui, 13: Font, s9
	}
	If InStr(params, "Level_3_GB_Local")
	{
		Gui, 13: Add, Text, w60 x90 y170 cBlue, Local
		Gui, 13: Add, Edit, w60 x90 y197 vLevel_3_GB_Local, %Level_3_GB_Local%
	}
	If InStr(params, "Level_3_GB_mAOD")
	{
		Gui, 13: Add, Text, w60 x160 y170 cBlue, mAOD
		Gui, 13: Add, Edit, w60 x160 y197 vLevel_3_GB_mAOD, %Level_3_GB_mAOD%
	}
	
	Gui, 13: Tab, Lvl 4
	Gui, 13: Font, s8
	If (Lvl_4_Internal_dipPlate_Datum || Lvl_4_External_dipPlate_Datum)
	{
		Gui, 13: Add, Text, w60 x20 y70, Dip Datum
		Gui, 13: Add, Text, w60 x20 y100, Dip Reading
		Gui, 13: Add, Text, w60 x20 y130, Water Level
	}
	Gui, 13: Font, s9
	If Lvl_4_Internal_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x90 y40 cBlue, Internal
		Gui, 13: Add, Edit, w60 x90 y67 vLvl_4_Internal_dipPlate_Datum ReadOnly, %Lvl_4_Internal_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x90 y97 vDip_4_Internal gCalc_Level_4_Internal, %Dip_4_Internal%
		Gui, 13: Add, Edit, w60 x90 y127 vLevel_4_Internal ReadOnly, %Level_4_Internal%
	}
	If Lvl_4_External_dipPlate_Datum
	{	
		Gui, 13: Add, Text, w60 x160 y40 cBlue, External
		Gui, 13: Add, Edit, w60 x160 y67 vLvl_4_External_dipPlate_Datum ReadOnly, %Lvl_4_External_dipPlate_Datum%
		Gui, 13: Add, Edit, w60 x160 y97 vDip_4_External gCalc_Level_4_External, %Dip_4_External%
		Gui, 13: Add, Edit, w60 x160 y127 vLevel_4_External ReadOnly, %Level_4_External%
	}
	If InStr(params, "Level_4_GB_Local") or InStr(params, "Level_4_GB_mAOD")
	{
		Gui, 13: Font, s8
		Gui, 13: Add, Text, w60 x20 y200, Staff Gauge
		Gui, 13: Font, s9
	}
	If InStr(params, "Level_4_GB_Local")
	{
		Gui, 13: Add, Text, w60 x90 y170 cBlue, Local
		Gui, 13: Add, Edit, w60 x90 y197 vLevel_4_GB_Local, %Level_4_GB_Local%
	}
	If InStr(params, "Level_4_GB_mAOD")
	{
		Gui, 13: Add, Text, w60 x160 y170 cBlue, mAOD
		Gui, 13: Add, Edit, w60 x160 y197 vLevel_4_GB_mAOD, %Level_4_GB_mAOD%
	}
	
	Gui, 13: Tab
}

;Add the control buttons
Gui, 13: Add, Button, w60 x10 y270 h20, Back
Gui, 13: Add, Button, w60 x170 y270 h20, Save
Gui, 13: Show, x0 y23 w240 h300, Outstation Data
SIPControl(1,0)
Return

;Calculation subroutines
;automatically calculate differences and apend to correct control
Calc_Level_GW:
Gui, 13: Submit, NoHide
GuiControl, 13: , Level_GW, % ((datum + groundwater_Datum_Offset) - Dip_Groundwater_Level)
CalcLevel("datum", "Dip_Groundwater_Level", "Level_GW", 13)
GuiControl, 13: , Level_GW_mBDAT, % Dip_Groundwater_Level
Return

Calc_Level_Head_Internal:
CalcLevel("head_Internal_dipPlate_Datum", "Dip_Head_Internal", "Level_Head_Internal", 13)
Return

Calc_Level_Head_External:
CalcLevel("head_External_dipPlate_Datum", "Dip_Head_External", "Level_Head_External", 13)
Return

Calc_Level_Tail_Internal:
CalcLevel("tail_Internal_dipPlate_Datum", "Dip_Tail_Internal", "Level_Tail_Internal", 13)
Return

Calc_Level_Tail_External:
CalcLevel("tail_External_dipPlate_Datum", "Dip_Tail_External", "Level_Tail_External", 13)
Return

Calc_Level_Crest:
CalcLevel("crest_dipPlate_Datum", "Dip_Crest", "Level_Crest", 13)
Return

Calc_Level_3_Internal:
CalcLevel("Lvl_3_Internal_dipPlate_Datum", "Dip_3_Internal", "Level_3_Internal", 13)
Return

Calc_Level_3_External:
CalcLevel("Lvl_3_External_dipPlate_Datum", "Dip_3_External", "Level_3_External", 13)
Return

Calc_Level_4_Internal:
CalcLevel("Lvl_4_Internal_dipPlate_Datum", "Dip_4_Internal", "Level_4_Internal", 13)
Return

Calc_Level_4_External:
CalcLevel("Lvl_4_External_dipPlate_Datum", "Dip_4_External", "Level_4_External", 13)
Return

;This subroutine checks the datums available and restricts which tabs can be viewed accordingly.
CheckDatums:
GuiControlGet, selectedRefTab, 13: , RefLevelTabs
If (((selectedRefTab = "Tail") && (!tail_Internal_dipPlate_Datum && !tail_External_dipPlate_Datum && !InStr(params, "Level_Tail_GB_Local") && !InStr(params, "Level_Tail_GB_mAOD"))) 
|| ((selectedRefTab = "Crest") && (!crest_dipPlate_Datum)) 
|| ((selectedRefTab = "Lvl 3") && (!Lvl_3_Internal_dipPlate_Datum && !Lvl_3_External_dipPlate_Datum && !InStr(params, "Level_3_GB_Local") && !InStr(params, "Level_3_GB_mAOD"))) 
|| ((selectedRefTab = "Lvl 4") && (!Lvl_4_Internal_dipPlate_Datum && !Lvl_4_External_dipPlate_Datum && !InStr(params, "Level_4_GB_Local") && !InStr(params, "Level_4_GB_mAOD"))))
{
	GuiControl, 13: ChooseString, RefLevelTabs, %RefLevelTabs%
}
Return

;Save button - As well as saving the input data, we also enable to Data collection button on Gui 10 as long as there are some reference levels
13ButtonSave:
Gui, 13: Submit
;as before, Groundwater is a special case
If (SiteType = "Groundwater")
{
	If Level_GW or Level_GW_mBDAT
	{
		GuiControl, 10: Enable, DataButton
	}
}
;For the other site Types we see if the necessary reference levels have been calculated
Else
{
	If ((head_Internal_dipPlate_Datum || head_External_dipPlate_Datum) 
	&& (Level_Head_External || Level_Head_Internal))
	{
		GuiControl, 10: Enable, DataButton
	}
	If ((tail_Internal_dipPlate_Datum || tail_External_dipPlate_Datum) 
	&& (Level_Tail_Internal || Level_Tail_External))
	{
		GuiControl, 10: Enable, DataButton
	}
	If ((crest_dipPlate_Datum) 
	&& (Level_Crest))
	{
		GuiControl, 10: Enable, DataButton
	}
	If ((Lvl_3_Internal_dipPlate_Datum || Lvl_3_External_dipPlate_Datum) 
	&& (Level_3_Head_External || Level_3_Head_Internal))
	{
		GuiControl, 10: Enable, DataButton
	}
	If ((Lvl_4_Internal_dipPlate_Datum || Lvl_4_External_dipPlate_Datum) 
	&& (Level_4_Head_External || Level_4_Head_Internal))
	{
		GuiControl, 10: Enable, DataButton
	}
	If ((Level_Head_GB_Local || Level_Head_GB_mAOD) 
	|| (Level_Tail_GB_mAOD || Level_Tail_GB_Local) 
	|| (Level_3_GB_Local || Level_3_GB_mAOD) 
	|| (Level_4_GB_Local || Level_4_GB_mAOD))
	{
		GuiControl, 10: Enable, DataButton
	}
	Else
	{
		GuiControl, 10: Disable, DataButton
	}
}
Gui, 13: Destroy
Gui, 10: Default
Gui, 10: Show
Return

;If the user hits the back button we clear all the collectable variables
13ButtonBack:
MsgBox, 36, FieldLogs Lite, You are about to wipe all data collected on this page.`n`nDo you want to continue?
IfMsgBox, No
{
	Return
}
Dip_Groundwater_Level = 
Level_GW =
Level_GW_mBDAT =
Dip_Head_External =
Level_Head_External =
Level_Head_GB_Local =
Level_Head_GB_mAOD =
Dip_Head_Internal =
Level_Head_Internal =
Dip_Tail_Internal = 
Level_Tail_Internal =
Dip_Tail_External =  
Level_Tail_External =
Level_Tail_GB_Local =
Level_Tail_GB_mAOD =
Dip_Crest =
Level_Crest = 
Dip_3_External =
Level_3_External =
Level_3_GB_Local =
Level_3_GB_mAOD =
Dip_3_Internal =
Level_3_Internal =
Dip_4_External =
Level_4_External =
Level_4_GB_Local =
Level_4_GB_mAOD =
Dip_4_Internal =
Level_4_Internal =
GuiControl, 10: Disable, DataButton
Gui, 13: Destroy
Gui, 10: Default
Gui, 10: Show
Return
