﻿;======================================================================================================================================
;
; FieldLogs Lite
;
; A windows CE implimentation of the field collection component of the Field Logs data base system
;
; This file is kept as an .ahk file as compiling reduces some necessary functionality.
; This file includes a number of other files. 
; These are all stored in the Program Directory on the Archer and to the end user will look like one program
;
;=======================================================================================================================

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
;SetWorkingDir \Program Files\FieldLogs Lite\  ; Ensures a consistent starting directory.
#SingleInstance, Force
DetectHiddenWindows, On

;##################################################################
;Set vars for testing
;

;siteType := "TBR"
;Goto, TBRDataEntry

;
;###################################################################

;This is the program directory. The check here is more for testing than actual field use. it doens't hurt to check though
If !FileExist("\Program Files\FieldLogs Lite")
{
	FileCreateDir, \Program Files\FieldLogs Lite
}
If FileExist("\My Documents\FieldLogs Lite Installer.exe")
{
	FileDelete, \My Documents\FieldLogs Lite Installer.exe
}

;#####################################################################################################################
;First things first, we should build the main GUI

;Set the tabs and site choose area
Gui, -Caption +Border
Gui, Add, Text, w220 y5 x10 vRound Center R1 -Wrap,
Gui, Add, ComboBox, w150 x10 y25 R15 Sort vsiteName gCheckSite,
Gui, Add, Button, w60 x170 y25 h20 vSiteLoadButton Disabled, Load
Gui, Add, Tab2, w220 h220 x10 y45 vMainTabs Disabled gCheckMainTabs, Details|Collect Data|Historic Data

;The details tab shows the information about the chosen site
Gui, Tab, Details
Gui, Add, Text, w90 x20 y90 Right, Site ID:
Gui, Add, Text, w90 x120 y90 Left vsiteID -Wrap,
Gui, Add, Text, w90 x20 y115 Right, Site Type:
Gui, Add, Text, w90 x120 y115 Left vsiteType -Wrap,
Gui, Add, Text, w90 x20 y140 Right, NGR:
Gui, Add, Text, w90 x120 y140 Left vNGR -Wrap,
Gui, Add, Text, w90 x20 y165 Right, Datum:
Gui, Add, Text, w90 x120 y165 Left vdatum -Wrap,
Gui, Add, Button, w200 x20 y200 h25 disabled vsiteDirectionsButton, Site Directions
Gui, Add, Button, w200 x20 y230 h25 disabled vSiteCommentsButton, Site Comments

;The collect data tab starts with the H&S checks before real data can be collected
Gui, Tab, Collect Data
Gui, Add, Text, w200 x20 y80 Center cBlue, STOP and THINK - Is the site safe?
Gui, Font, s7
Gui, Add, Text, w180 x30 y97 Center R2, Look at the hazard sheet and carry out a dynamic risk assessment.
Gui, Font, s9
Gui, Add, Radio, w80 x30 y130 Right cGreen vDRAGreen gDRA, Green
Gui, Add, Radio, w80 x130 y130 cRed vDRARed gDRA, Red
Gui, Add, Text, w200 x20 y150 Center cBlue, Are there Public Safety concerns?
Gui, Font, s7
Gui, Add, Text, w180 x30 y167 Center R2, Is there anything on site that is a danger to the public? Check PSRA.
Gui, Font, s9
Gui, Add, Radio, w80 x30 y200 Right cGreen vPSRAGreen gPSRA, Green
Gui, Add, Radio, w80 x130 y200 cRed vPSRARed gPSRA, Red
Gui, Add, Button, w200 x20 y225 h30 Disabled vDataCollectOKButton, OK

;the Historic data tab displays the historic data for the selected site (only the last three visits are available on this Lite version
Gui, Tab, Historic Data
Gui, Add, Text, w200 x20 y80 Center vVisit1,
Gui, Add, Button, w80 x30 y100 vVisit1Comments gHistoricComments Disabled, Comments
Gui, Add, Button, w80 x130 y100 vVisit1Data gHistoricData Disabled, Data
Gui, Add, Text, w200 x20 y140 Center vVisit2,
Gui, Add, Button, w80 x30 y160 vVisit2Comments gHistoricComments Disabled, Comments
Gui, Add, Button, w80 x130 y160 vVisit2Data gHistoricData Disabled, Data
Gui, Add, Text, w200 x20 y200 Center vVisit3,
Gui, Add, Button, w80 x30 y220 vVisit3Comments gHistoricComments Disabled, Comments
Gui, Add, Button, w80 x130 y220 vVisit3Data gHistoricData Disabled, Data

;At the bottom of the page are the two buttons
Gui, Tab
Gui, Add, Button, w60 x10 y270 h20, Settings
Gui, Add, Button, w60 x170 y270 h20, Close

;We show the GUI. The width and Height are the full height of the Archer screen with an offset for the top bar.
;The two bottom buttons are common to every page but have a space between them for the SIP keyboard button
Gui, Show, Hide x0 y23 w240 h300, FieldLogs Lite

;the LoadRound() Function scans the $round.xml file specified in the settings and displays the available sites in the dropdown at the top of the window
LoadRound()
;The SIPControl() function gives control over the SIP Keyboard which can be hidden, shown and have the keyboard shown or hidden too.
SIPControl(1) ;Show SIP button
Return

;#####################################################################################################################
;Quit the program when the Gui is closed or the close button is pressed

ButtonClose:
GuiClose:
ExitApp

;#####################################################################################################################
;Hide the SIP keyboard when the tabs are changed as it obscures the screen

CheckMainTabs:
SIPControl(1, 0)
Return

;#####################################################################################################################
;#####################################################################################################################
;When a new site is chosen we want to start afresh by clearing any data which may have already been collected
;This is the gLabel of the Site list combobox

CheckSite:
Gui, Submit, NoHide
;the SiteNamesMatchList variable is a comma separated list of the available sites in the chosen round. 
;It is created by the LoadRound() function
;We perform this check as the user can enter free text into the combobox as a pseudo-search but we only want to react if we are sure the site exists as an xml file
If siteName in %siteNamesMatchList%
{
	;If the site is good we allow the site data to be loaded (Enable SiteLoadButton) and clear any existing, site specific UI data
	GuiControl, Enable, SiteLoadButton
	DRAGreen := ""
	GuiControl, , DRAGreen, 0
	DRARed := ""
	GuiControl, , DRARed, 0
	DRAReson := ""
	PSRAGreen := ""
	GuiControl, , PSRAGreen, 0
	PSRARed := ""
	GuiControl, , PSRARed, 0
	PSRAReason := ""
	GuiControl, Disable, DataCollectOKButton
	GuiControl, , siteID
	GuiControl, , siteType
	GuiControl, , NGR
	GuiControl, , datum
	SiteDirections := ""
	TAR_Site_Comments := ""
	GuiControl, ChooseString, MainTabs, Details
	GuiControl, Disable, MainTabs
}
;If the site isn;t in the list, we disable to load button so that no error occur from users trying to load data for non-existant sites
Else
{
	GuiControl, Disable, SiteLoadButton
}
Return


;#####################################################################################################################
;When the user selects a site, we load the necessary data into the Gui Details tab
;We have already made sure that the site is in the list of recognised sites so this should go without a hitch

ButtonLoad:
;Submit the Gui to get the name of the site from the combobox
Gui, Submit, NoHide
;Backout if there is no site name (theoretically impossible but safer just to check)
If !siteName
{
	Return
}
;When the user makes a site selection from the drop down list we get the site ID from the $round.xml file
;Disable the Tabs and ComboBox so that data cant accidentally be changed during the update
GuiControl, MoveDraw, MainTabs
GuiControl, Disable, MainTabs
GuiControl, MoveDraw, siteName
GuiControl, Disable, siteName
;We disable these buttons so that they can't be accidentaly pressed
Guicontrol, Disable, siteDirectionsButton
Guicontrol, Disable, siteCommentsButton
;Loop through the $round.xml file and get the siteID from the line which has the siteName in it.
Loop, Read, % XMLDir . "\$round.xml"
{
	If InStr(A_LoopReadLine, siteName)
	{	
		If !InStr(A_LoopReadLine, "fr:Site")
		{
			Continue
		}
		;We read through the file line by line and extract the site ID from the xml
		;We use the StrX function for parsing the xml
		;See http://www.autohotkey.com/board/topic/47368-strx-auto-parser-for-xml-html/
		siteID := StrX(A_LoopReadLine, "siteID", 1, 8, "siteName", True, 10, N)
		;Once we have the SiteID we can break out of this loop
		Break
	}
	Continue
}
;Read the site specific xml file into memory
FileRead, siteXML, % XMLDir . "\" . siteID . ".xml"
;finding that this string exists in the site xml file indicates that data has already been collected for the site. 
If InStr(siteXML, "xmlns:ns")
{	
	;alert the user
	Msgbox, 16, FieldLogs Lite, Data has already been collected for %siteName%.`nPlease load a new $round.xml file to collect data 
	ClearGui()
	;enable all the controls again so that data can be collected
	Guicontrol, Enable, siteDirectionsButton
	Guicontrol, Enable, siteCommentsButton
	GuiControl, Enable, siteName
	Return
}
;We can update the main Gui to show the site details gathered from the xml file 
GuiControl, , siteID, % siteID
GuiControl, , siteType, % siteType := StrX(siteXML, "<fl:SiteType>", 1, 13, "</fl:SiteType>", True, 14, N)
GuiControl, , NGR, % NGR := StrX(siteXML, "<fl:NGR>", 1, 8, "</fl:NGR>", True, 9, N)
GuiControl, , datum, % datum := Round(StrX(siteXML, "<fl:datum>", 1, 10, "</fl:datum>", True, 11, N), 3)
;Above are the details we display on the front page.
;We also need to collect the other details from the site xml file. 
;We may as well do that here
roundPosition := StrX(siteXML, "<fl:roundPosition>", 1, 18, "</fl:roundPosition>", True, 19, N)
SiteDirections := StrX(siteXML, "<fl:SiteDirections>", 1, 19, "</fl:SiteDirections>", True, 20, N)
TAR_SiteComments :=	StrX(siteXML, "<fl:TAR_SiteComment>", 1, 20, "</fl:TAR_SiteComment>", True, 21, N)
logOfficer := StrX(siteXML, "<fl:logOfficer>", 1, 15, "</fl:logOfficer>", True, 16, N)
logDipper := StrX(siteXML, "<fl:logDipper>", 1, 14, "</fl:logDipper>", True, 15, N)
keys := StrX(siteXML, "<fl:keys>", 1, 9, "</fl:keys>", True, 10, N)
head_Internal_dipPlate_Datum := StrX(siteXML, "<fl:head_Internal_dipPlate_Datum>", 1, 33, "</fl:head_Internal_dipPlate_Datum>", True, 34, N)
head_External_dipPlate_Datum := StrX(siteXML, "<fl:head_External_dipPlate_Datum>", 1, 33, "</fl:head_External_dipPlate_Datum>", True, 34, N)
tail_Internal_dipPlate_Datum := StrX(siteXML, "<fl:tail_Internal_dipPlate_Datum>", 1, 33, "</fl:tail_Internal_dipPlate_Datum>", True, 34, N)
tail_External_dipPlate_Datum := StrX(siteXML, "<fl:tail_External_dipPlate_Datum>", 1, 33, "</fl:tail_External_dipPlate_Datum>", True, 34, N)
crest_dipPlate_Datum := StrX(siteXML, "<fl:crest_dipPlate_Datum>", 1, 25, "</fl:crest_dipPlate_Datum>", True, 26, N)
Lvl_3_Internal_dipPlate_Datum := StrX(siteXML, "<fl:Lvl_3_Internal_dipPlate_Datum>", 1, 34, "</fl:Lvl_3_Internal_dipPlate_Datum>", True, 35, N)
Lvl_3_External_dipPlate_Datum := StrX(siteXML, "<fl:Lvl_3_External_dipPlate_Datum>", 1, 34, "</fl:Lvl_3_External_dipPlate_Datum>", True, 35, N)
Lvl_4_Internal_dipPlate_Datum := StrX(siteXML, "<fl:Lvl_4_Internal_dipPlate_Datum>", 1, 34, "</fl:Lvl_4_Internal_dipPlate_Datum>", True, 35, N)
Lvl_4_External_dipPlate_Datum := StrX(siteXML, "<fl:Lvl_4_External_dipPlate_Datum>", 1, 34, "</fl:Lvl_4_External_dipPlate_Datum>", True, 35, N)
groundwater_Datum_Offset := StrX(siteXML, "<fl:groundwater_Datum_Offset>", 1, 29, "</fl:groundwater_Datum_Offset>", True, 30, N)
currentVisitFrequency := StrX(siteXML, "<fl:currentVisitFrequency>", 1, 26, "</fl:currentVisitFrequency>", True, 27, N)
redCard := StrX(siteXML, "<fl:redCard>", 1, 12, "</fl:redCard>", True, 13, N)

;We then enable the Historic Data buttons and fill in the necessary data
;The data is in Ini files, one for each site which contain three sections, one for each past visit.
;These are numbered 1,2 and 3 so that we can use the A_Index loop variable to retrieve the data
Loop, 3
{
	If FileExist("\My Documents\FieldLogs Lite\Historic\" . SiteID . ".ini")
	{
		;We get the officer and date time data
		IniRead, Officer, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %A_Index%, logOfficer, None
		IniRead, DateTime, \My Documents\FieldLogs Lite\Historic\%SiteID%.ini, %A_Index%, logDateTime, None
		;If a visit has no datetime and no officer it is likely a NO_Site visit (ie. near neighbour checks were done instead)
		;As this is the case there will be no data to display so we disable the option to view that vist
		;The query that build the <siteNo.>.ini file does check for NO_Site visits but some may slip through. best to check
		If (Officer = "None") and (DateTime = "None")
		{
			GuiControl, , Visit%A_Index%
			GuiControl, Disable, Visit%A_Index%
			GuiControl, Disable, Visit%A_Index%Comments
			GuiControl, Disable, Visit%A_Index%Data
		}
		Else
		{
			GuiControl, , Visit%A_Index%, %Officer% - %DateTime%
			GuiControl, Enable, Visit%A_Index%
			GuiControl, Enable, Visit%A_Index%Comments
			GuiControl, Enable, Visit%A_Index%Data
		}
	}
}

;enable all the controls again so that data can be collected
Guicontrol, Enable, siteDirectionsButton
Guicontrol, Enable, siteCommentsButton
GuiControl, Enable, MainTabs
GuiControl, Enable, siteName

;Warn the user if the site has been red carded
If redCard
{
	MsgBox, 16, FieldLogs Lite, %siteName% was RED CARDED on %redCard%.`n`n%TAR_SiteComments%
	;We don't clear the UI as the user may want to view past data or even collect data if the Red Card is flagged accidentally
}

Return


#Include, \Program Files\FieldLogs Lite\functions.ahk ;contains all the functions used by the script
#Include, \Program Files\FieldLogs Lite\hands.ahk ;Health and safety code. Also has the site comments and directions code
#Include, \Program Files\FieldLogs Lite\historic.ahk ; cointains the code which allows historic data to be viewed
#Include, \Program Files\FieldLogs Lite\mainui.ahk ;the main UI for data collection
#Include, \Program Files\FieldLogs Lite\paramdata.ahk ;The UI for collecting editable site parameters
#Include, \Program Files\FieldLogs Lite\refdata.ahk ;The UI for collecting Reference data details
#Include, \Program Files\FieldLogs Lite\settings.ahk ;contains all the code which allow the settings to be changed. Also contains the Parameter editting code
#Include, \Program Files\FieldLogs Lite\tbrdata.ahk ;The UI for collecting Rain gauge data



#z::reload ; a handy shortcut for reloading the script during testing