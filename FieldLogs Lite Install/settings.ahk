﻿;#####################################################################################################################
;#####################################################################################################################
;Settings code goes here
;The settings allow the user to change the round if they have multiple $round.xml files
;They can also Edit the error values for thigs like battery voltage and oiutstation differences. 
;These will allow different users/regions etc. to set their own tolerances

;Users can also edit Parameters. This allows them to add/remove the xml tags which are needed for each site.
;As users can edit the tags on the main database, it is theoretically possible that a site will exist that we can't collect all the data for.
;The parameters settings aims to remove that by allowing extra parameters to be added to different site classes.
;The implementation is pretty rough. After the normal, scripted data has been collected, the script checks for extra parameters and displays an input box for each.
;obviously, if someone adds 500 new parameters to a site class, it will take a long time to collect all the data but this is only really intended for one or two extras to be added.
;The aim is to provide some level of independance until an update to the FiledLogs Lite script can be made. 


ButtonSettings:
;We track changes so that we only save if changes are made
Changes =
SIPControl(0) ;Hide SIP button and keyboard
Gui, 6: -Caption +Border
;XML location - allow user to change to a different $round.xml file 
Gui, 6: Add, Text, w150 x10 y10 Left cBlue, Location of xml files
Gui, 6: Add, Button, w60 x170 y7 h20 gChooseXMLDir, Change
IniRead, XMLDir, \Program Files\FieldLogs Lite\settings.ini, settings, xmldir, \My Documents\FieldLogs Lite
Gui, 6: Add, Edit, w220 x10 y30 vXMLDirEdit ReadOnly R2, % XMLDir

;Values
Gui, 6: Add, Text, w220 x10 y70 Center cBlue, Error Values
IniRead, DCVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, dcvolts, 30
Gui, 6: Add, Text, w100 x10 y95 Right, DC Volts:
Gui, 6: Add, Edit, w80 x120 y92 vDCVoltsValue gSetChanges, % DCVoltsValue
IniRead, InternalVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, internalvolts, 30
Gui, 6: Add, Text, w100 x10 y120 Right, Internal Volts:
Gui, 6: Add, Edit, w80 x120 y117 vInternalVoltsValue gSetChanges, % InternalVoltsValue
IniRead, LoggerVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, loggervolts, 12
Gui, 6: Add, Text, w100 x10 y145 Right, Logger Volts:
Gui, 6: Add, Edit, w80 x120 y142 vLoggerVoltsValue gSetChanges, % LoggerVoltsValue
IniRead, OSErrorValue, \Program Files\FieldLogs Lite\settings.ini, values, oserror, 0.005
Gui, 6: Add, Text, w100 x10 y170 Right, OS Error:
Gui, 6: Add, Edit, w80 x120 y167 vOSErrorValue gSetChanges, % OSErrorValue
IniRead, LoggerErrorValue, \Program Files\FieldLogs Lite\settings.ini, values, loggererror, 0.005
Gui, 6: Add, Text, w100 x10 y195 Right, Logger Error:
Gui, 6: Add, Edit, w80 x120 y192 vLoggerErrorValue gSetChanges, % LoggerErrorValue

;Parameters
Gui, 6: Add, Button, w100 x10 y225 h30, Edit Parameters
;Station details
Gui, 6: Add, Button, w100 x130 y225 h30, OS Settings

;Navigation buttons
Gui, 6: Add, Button, w60 x10 y270 h20, Back
Gui, 6: Add, Button, w60 x170 y270 h20, Save
Gui, 6: Show, x0 y23 w240 h300, Settings
SIPControl(1)
Return

;This is the gLabel for each of the error tolerance edit boxes. 
;making a change to any of them will mean Changes = 1
SetChanges:
Changes = 1
Return

;The back button is pressed.
;Check if changes should be saved
6ButtonBack:
If Changes
{
	MsgBox, 20, FieldLogs Lite, Changes have been made.`nWould you like to save them?
	IfMsgBox, Yes
	{
		GoSub, 6ButtonSave
		Return
	}
}
Gui, 6: Destroy
Gui, 1: Default
Gui, 1: Show
Return

;Save the changes.
;quicker to just save everything
6ButtonSave:
Gui, 6: Submit, NoHide
IniWrite, % XMLDirEdit, \Program Files\FieldLogs Lite\settings.ini, settings, xmldir
IniWrite, % DCVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, dcvolts
IniWrite, % InternalVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, internalvolts
IniWrite, % LoggerVoltsValue, \Program Files\FieldLogs Lite\settings.ini, values, loggervolts
IniWrite, % OSErrorValue, \Program Files\FieldLogs Lite\settings.ini, values, oserror
IniWrite, % LoggerErrorValue, \Program Files\FieldLogs Lite\settings.ini, values, loggererror
Gui, 6: Destroy
Gui, 1: Default
Gui, 1: Show
MsgBox, 0, FieldLogs Lite, Settings have been saved
LoadRound()
Return

;This code allows the user to specify a new location for their round xml files
;it will be saved and set as default next time 
ChooseXMLDir:
MsgBox, 64, FieldLogs Lite, Please navigate to the $round.xml file you want to use.`nIt may take a while to load all xml files so please be patient with the next window.
;File select winodw on CE is SLOOOOOOW!
FileSelectFile, XMLDir, 1, , Choose $round.xml, xml (*.xml)
SIPControl(0)
;If the user hit cancel
If (ErrorLevel > 0)
{
	;we cancel
	Gui, 6: Show
	Return
}
;otherwise we can save their data
SplitPath, XMLDir, , XMLDir
GuiControl, 6: , XMLDirEdit, %XMLDir%
Changes = 1
Gui, 6: Show
Return

;######################################################################
;Parameters can be edited to extend functionality beyond what is coded here

;The UI could use some prettifying at some point in the future
6ButtonEditParameters:
;Give the user fair warning that they can break their install by messing with this
MsgBox, 16, FieldLogs Lite, Editing the parameters can break the data collection functionality.`nPlease only edit if you know what you are doing, 5
Gui, 7: -Caption +Border
Gui, 7: Add, Text, w220 x10 y10 Center cBlue, 1. Choose a site type
Gui, 7: Add, DDL, w220 x10 y30 vParam_siteType Sort gLoadParams R7, Flow Structures|Flow Ultrasonic|Flow Electromagnetic|Flow Velocity-Index|Level|TBR|Groundwater|Not Listed Params
Gui, 7: Add, Text, w220 x10 y65 Center cBlue, 2. Choose a parameter to edit
Gui, 7: Add, DDL, w220 x10 y85 vParam_parameter Sort R10 Disabled,
Gui, 7: Add, Button, w100 x20 y115 Disabled vParam_edit, Edit
Gui, 7: Add, Button, w100 x120 y115 Disabled vParam_delete, Delete
Gui, 7: Add, Edit, w150 x20 y155 Disabled vParam_editParam
Gui, 7: Add, Button, w40 x180 y154 vParam_save Disabled, Save
Gui, 7: Add, Text, w220 x10 y200 Center cBlue, Add a new parameter
Gui, 7: Add, Edit, w170 x10 y220 vParam_addParam
Gui, 7: Add, Button, w40 x190 y219, Add
Gui, 7: Add, Button, w60 x10 y270 h20, Back
Gui, 7: Show, x0 y23 w240 h300, Edit Parameters
SIPControl(1)
Return

;Load the parameters for the chosen site type
LoadParams:
Gui, 7: Submit, NoHide
StringLower, Param_siteType, Param_siteType
StringReplace, Param_siteType, Param_siteType, %A_Space%, , A
StringReplace, Param_siteType, Param_siteType, -, , A
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, %Param_siteType%
GuiControl, 7: Enable, Param_parameter
GuiControl, 7: Enable, Param_edit
GuiControl, 7: Enable, Param_delete
GuiControl, 7: , Param_parameter, % "|" . params 
Return

;Back out of this UI
7ButtonBack:
Gui, 7: Destroy
Gui, 6: Default
Gui, 6: Show
Return

;To edit the parameter we pull the parameter name to the eidt box and enable the buttons which are needed
7ButtonEdit:
Gui, 7: Submit, NoHide
GuiControl, 7: Enable, Param_editParam
GuiControl, 7: Enable, Param_save
GuiControl, 7: , Param_editParam, % Param_parameter
Return

;Deleting a parameter
7ButtonDelete:
Gui, 7: Submit, NoHide
;Warn the user and allow them to back out if they pressed this in error
MsgBox, 36, FieldLogs Lite, Are you sure you want to delete %Param_parameter%?
IfMsgBox, No
{
	Return
}
;To delete the parameter we loop through the list of existing params and copy each to a new list.
;If we come across the parameter to delete we skip adding it to the new list
new_params := ""
Loop, Parse, params, |
{
	If (A_LoopField = Param_parameter)
	{
		Continue
	}
	new_params .= A_LoopField . "|"
}
;make sure the list doesn;t have a trailing pipe and save iot back to the settings.ini file
StringTrimRight, new_params, new_params, 1
IniWrite, %new_params%, \Program Files\FieldLogs Lite\settings.ini, params, %Param_siteType%
;Clear and Disbale the necessry GUI elements
GuiControl, 7: , Param_editParam
GuiControl, 7: Disable, Param_editParam
GuiControl, 7: Disable, Param_save
;Get the new list from the Ini file. This ensures that the list is definitely the one from the ini file
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, % Param_siteType, %A_Space%
;Load it back into the drop down
GuiControl, 7: , Param_parameter, % "|" . params
;Let the user know we're done 
MsgBox, 0, FieldLogs Lite, "%Param_parameter%" has been deleted, 3
Return

;The save button is pressed when a new parameter has been edited
7ButtonSave:
;submit to get the new name
Gui, 7: Submit, NoHide
;similar to above we loop through the existing parameters and save the new name instead of the old one into a new list
new_params := ""
Loop, Parse, params, |
{
	If (A_LoopField = Param_parameter)
	{
		new_params .= Param_editParam . "|"
		Continue
	}
	new_params .= A_LoopField . "|"
}
StringTrimRight, new_params, new_params, 1
IniWrite, % new_params, \Program Files\FieldLogs Lite\settings.ini, params, % Param_siteType
GuiControl, 7: , Param_editParam
GuiControl, 7: Disable, Param_editParam
GuiControl, 7: Disable, Param_save
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, % Param_siteType, %A_Space%
GuiControl, 7: , Param_parameter, % "|" . params 
MsgBox, 0, FieldLogs Lite, Changes saved, 3
Return

;The add button adds a new parameter to the end of the params list
7ButtonAdd:
Gui, 7: Submit, NoHide
StringReplace, Param_addParam, Param_addParam, %A_Space%, _, A
IniWrite, % params . "|" . Param_addParam, \Program Files\FieldLogs Lite\settings.ini, params, % Param_siteType
;Once it's saved we update the UI sp that the new param can be edited or deleted
GuiControl, 7: , Param_addParam
IniRead, params, \Program Files\FieldLogs Lite\settings.ini, params, % Param_siteType, %A_Space%
GuiControl, 7: , Param_parameter, % "|" . params 
MsgBox, 0, FieldLogs Lite, "%Param_addParam%" has been added, 3
Return


;=================================================================================================================
;
; OS Settings - This allows certain Out Station specific settings to be changed

6ButtonOSSettings:

Gui, 9: -Caption +Border
Gui, 9: Add, Text, w220 x20 y30 cBlue Center, OS Settings
Gui, 9: Add, Text, w70 x20 y70 Right, Ground water:
Gui, 9: Add, DDL, w90 x100 y67 vgroundWaterMeasure, mAOD|mBDAT
IniRead, groundWaterMeasure, \Program Files\FieldLogs Lite\settings.ini, ossettings, groundWaterMeasure, mAOD
GuiControl, 9: ChooseString, groundWaterMeasure, % groundWaterMeasure
Gui, 9: Add, Text, w70 x20 y100 Right, TBR :
Gui, 9: Add, DDL, w90 x100 y97 vtbrMeasure, Count|Cum. Total
IniRead, tbrMeasure, \Program Files\FieldLogs Lite\settings.ini, ossettings, tbrMeasure, Count
GuiControl, 9: ChooseString, tbrMeasure, % tbrMeasure

;Navigation buttons
Gui, 9: Add, Button, w60 x10 y270 h20, Back
Gui, 9: Add, Button, w60 x170 y270 h20, Save
Gui, 9: Show, x0 y23 w240 h300, Settings
SIPControl(1)
Return

9ButtonSave:
Gui, 9: Submit
IniWrite, %groundWaterMeasure%, \Program Files\FieldLogs Lite\settings.ini, ossettings, groundWaterMeasure
IniWrite, %tbrMeasure%, \Program Files\FieldLogs Lite\settings.ini, ossettings, tbrMeasure
9ButtonBack:
Gui, 9: Destroy
Gui, 6: Default
Gui, 6: Show
Return