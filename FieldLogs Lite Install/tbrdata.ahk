﻿;#####################################################################################################################
;#####################################################################################################################
TBRDataEntry:
;
; Raingauges are a special case and deserve their own UI
;


Gui, 15: -Caption +Border
Gui, 15: Add, Tab2, w220 h250 x10 y10 vRainGaugeTabs, TBR|Weigh Gauge|Maint|Log	
	
Gui, 15: Tab, TBR
Gui, 15: Font, s8
Gui, 15: Add, Text, w70 x20 y50, Check Gauge
Gui, 15: Add, Edit, w110 x100 y48 vTBR_Check_Gauge_Total gCheck_Gauge_Actions, 
Gui, 15: Add, Text, w50 x110 y85 cBlue, TBR 1
Gui, 15: Add, Text, w50 x170 y85 cBlue, TBR 2

Gui, 15: Add, Text, w60 x20 y110 vTBR_Prev_Label,
Gui, 15: Add, Edit, w60 x90 y107 vTBR_Previous_Count, 
Gui, 15: Add, Edit, w60 x160 y107 vTBR_2_Previous_Count, 
Gui, 15: Add, Text, w60 x20 y140 vTBR_Curr_Label,
Gui, 15: Add, Edit, w60 x90 y137 vTBR_Current_Count gCheck_Gauge_Actions, 
Gui, 15: Add, Edit, w60 x160 y137 vTBR_2_Current_Count gCheck_Gauge_Actions,
Gui, 15: Add, Text, w60 x20 y170, Increment
Gui, 15: Add, Edit, w60 x90 y167 vTBR_Total, 
Gui, 15: Add, Edit, w60 x160 y167 vTBR_2_Total,
Gui, 15: Add, Text, w60 x20 y200, Diff. (mm)
Gui, 15: Add, Edit, w60 x90 y197 vTBR_Error, 
Gui, 15: Add, Edit, w60 x160 y197 vTBR_2_Error,
Gui, 15: Add, Text, w60 x20 y230, Var. (`%)
Gui, 15: Add, Edit, w60 x90 y227 vTBR_Percentage_Error, 
Gui, 15: Add, Edit, w60 x160 y227 vTBR_2_Percentage_Error,

Gui, 15: Tab, Weigh Gauge
Gui, 15: Add, Text, w70 x20 y50, Check Gauge
Gui, 15: Add, Edit, w110 x100 y48 vWeigh_Gauge_Check ReadOnly, 
Gui, 15: Add, Text, w70 x20 y80 vWeigh_Prev_Label,
Gui, 15: Add, Edit, w110 x100 y77 vTBR_WeighGauge_Previous_Count 
Gui, 15: Add, Text, w70 x20 y110 vWeigh_Curr_Label,
Gui, 15: Add, Edit, w110 x100 y107 vTBR_WeighGauge_Current_Count gCheck_Gauge_Actions, 
Gui, 15: Add, Text, w70 x20 y140, Increment
Gui, 15: Add, Edit, w110 x100 y137 vTBR_WeighGauge_Calibration_Check, 
Gui, 15: Add, Text, w70 x20 y170, Diff. (mm)
Gui, 15: Add, Edit, w110 x100 y167 vTBR_WeighGauge_Error,
Gui, 15: Add, Text, w70 x20 y200, Var. (`%)
Gui, 15: Add, Edit, w110 x100 y197 vTBR_WeighGauge_Percentage_Error,  
Gui, 15: Add, Text, w70 x20 y230, Impulse
Gui, 15: Add, Edit, w110 x100 y227 vTBR_WeighGauge_Impulse_Total,

Gui, 15: Tab, Maint
Gui, 15: Add, Text, w50 x110 y50 cBlue, TBR 1
Gui, 15: Add, Text, w50 x110 y50 cBlue, TBR 2
Gui, 15: Add, Text, w60 x20 y50, Test Tips
Gui, 15: Add, Edit, w60 x90 y47 vTBR_TestTips,
Gui, 15: Add, Edit, w60 x160 y47 vTBR_2_TestTips,
Gui, 15: Add, Text, w60 x20 y160, Cleaned
Gui, 15: Add, DDL, w60 x90 y157, Yes|No


Gui, 15: Tab, Log
Gui, 15: Add, Text, w50 x110 y50 cBlue, TBR 1
Gui, 15: Add, Text, w50 x170 y50 cBlue, TBR 2
Gui, 15: Add, Text, w60 x20 y80, Logger
Gui, 15: Add, Edit, w60 x90 y77 vTBR_Logger_Total,
Gui, 15: Add, Edit, w60 x160 y77 vTBR_2_Logger_Total,
Gui, 15: Add, Text, w110 x100 y125 cBlue Center, Environment
Gui, 15: Add, Text, w70 x20 y160, Temperature
Gui, 15: Add, Edit, w120 x100 y157 vTBR_Temperature,
Gui, 15: Add, Text, w70 x20 y190, Temp Check
Gui, 15: Add, Edit, w120 x100 y187 vTBR_Temperature_Check,
Gui, 15: Add, Text, w70 x20 y220, Wind
Gui, 15: Add, Edit, w120 x100 y217 vTBR_Wind,


;The user can set wether their outstations are set to show Counts or Cum. Totals
IniRead, tbrMeasure, \Program Files\FieldLogs Lite\settings.ini, ossettings, tbrMeasure
;We can change some of the wording depending on which is set 
If (tbrMeasure = "Count")
{
	GuiControl, 15: , TBR_Prev_Label, Prev. Count
	GuiControl, 15: , TBR_Curr_Label, Curr. Count
	GuiControl, 15: , Weigh_Prev_Label, Prev. Count
	GuiControl, 15: , Weigh_Curr_Label, Curr. Count
}
If (tbrMeasure = "Cum. Total")
{
	GuiControl, 15: , TBR_Prev_Label, Prev. Total
	GuiControl, 15: , TBR_Curr_Label, Curr. Total
	GuiControl, 15: , Weigh_Prev_Label, Prev. Total 
	GuiControl, 15: , Weigh_Curr_Label, Curr. Total
}

;Add the control buttons at the bottom of the UI
Gui, 15: Tab
Gui, 15: Add, Button, w60 x10 y270 h20, Back
Gui, 15: Add, Button, w60 x170 y270 h20, Save
Gui, 15: Show, x0 y23 w240 h300, Outstation Data
SIPControl(1,0)

Return

;#####################################################################################################################
;There are various things we need to do when the check gauge value is changed.
;we need to synchronise the two 'Check Gauge' edit fields to show the same value so that the check value is visiable on the Weigh Gauge tab
;we also need to calculate the Current Counts and Difference/Variance figures for the TBRs 1/2 and WeighGauge  

Check_Gauge_Actions:
;Submit the Gui to get the ficures as they stand
Gui, 15: Submit, NoHide
;synchronise the Check Gauge values
GuiControl, 15: , Weigh_Gauge_Check, % TBR_Check_Gauge_Total

;Calculate the totals for the values currently given
;These calculatoins vary depending on whether the gauge is set up to show 'Count' or 'Cumulative Total"
If (tbrMeasure = "Count")
{
	TBR_Total 					:= Round(((TBR_Current_Count - TBR_Previous_Count)*0.2), 2)
	TBR_2_Total 				:= Round(((TBR_2_Current_Count - TBR_2_Previous_Count)*0.2), 2)
	TBR_WeighGauge_Calibration_Check	:= Round(((TBR_WeighGauge_Current_Count - TBR_WeighGauge_Previous_Count)*0.2), 2)
}
If (tbrMeasure = "Cum. Total")
{
	TBR_Total 					:= Round((TBR_Current_Count - TBR_Previous_Count), 2)
	TBR_2_Total 				:= Round((TBR_2_Current_Count - TBR_2_Previous_Count), 2)
	TBR_WeighGauge_Calibration_Check	:= Round((TBR_WeighGauge_Current_Count - TBR_WeighGauge_Previous_Count), 2)
}
;act if there are true totals
;First we check the value of the check gauge as calculatoins are different
;If the check gauge is less than or equal to 25mm 
If ((TBR_Total != "") && (TBR_Total > 0))
{
	GuiControl, 15: , TBR_Total, % TBR_Total
	GuiControl, 15: , TBR_Error, % Round((TBR_Total - TBR_Check_Gauge_Total), 2)
	GuiControl, 15: , TBR_Percentage_Error, % Round((((TBR_Total - TBR_Check_Gauge_Total)/TBR_Check_Gauge_Total)*100), 3)
}
If ((TBR_2_Total != "") && (TBR_2_Total > 0))
{
	GuiControl, 15: , TBR_2_Total, % TBR_2_Total
	GuiControl, 15: , TBR_2_Error, % Round((TBR_2_Total - TBR_Check_Gauge_Total), 2)
	GuiControl, 15: , TBR_2_Percentage_Error, % Round((((TBR_2_Total - TBR_Check_Gauge_Total)/TBR_Check_Gauge_Total)*100), 3)
}
If ((TBR_WeighGauge_Calibration_Check != "") && (TBR_WeighGauge_Calibration_Check > 0))
{
	GuiControl, 15: , TBR_WeighGauge_Calibration_Check, % TBR_WeighGauge_Calibration_Check
	GuiControl, 15: , TBR_WeighGauge_Error, % Round((TBR_WeighGauge_Calibration_Check - TBR_Check_Gauge_Total), 2)
	GuiControl, 15: , TBR_WeighGauge_Percentage_Error, % Round((((TBR_WeighGauge_Calibration_Check - TBR_Check_Gauge_Total)/TBR_Check_Gauge_Total)*100), 3)
}
Return



;#####################################################################################################################
;
; Actions for the control buttons at the bottom of GUI 15

15ButtonSave:
Gui, 15: Submit
Gui, 15: Destroy
Gui, 10: Default
Gui, 10: Show
SIPControl(1)
Return

15ButtonBack:
MsgBox, 36, FieldLogs Lite, You are about to wipe all the data from this page`n`nDo you want to continue?
IfMsgBox, No
{
	Return
}
Gui, 15: Destroy
Gui, 10: Default
Gui, 10: Show
SIPControl(1)
Return



/*

==================================TODO

Check Gauge = CheckGauge_Weigh

If check gauge < 25mm, 
	change tag 'variance' to 'difference' 
	also change equation to calulate that figure
	
If 'tag option' isCount = 1
	figures are true counts and need to be *0.2
Else
	figures are cumulative totals and don't need *0.2
	

logger can be environmental as no calculations are based on it.
