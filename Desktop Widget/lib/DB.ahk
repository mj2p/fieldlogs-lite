;=======================================
; Write Data to the database

DB_Write(SQL) {
	global DB
	ErrorLevel = 
	pcn := ComObjCreate("ADODB.Connection") 
	pcn.ConnectionString := "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" . DB . ";" 
	pcn.Open() 
	If (pcn.State != 1) 
	{ 
		ErrorLevel := "The connection to the database failed" 
		DB_Error(SQL, ErrorLevel)
	} 
	pcn.Execute(SQL) 
	pcn.Close() 
	If (ErrorLevel := ADO_GetError(pcn)) 
	{ 
		DB_Error(SQL, ErrorLevel)
	} 
	Else 
	{ 
		Return 0 
	}    
} 

;=====================================
; Read data from the database

DB_Read(SQL, sNames=0) {
	global DB
	ErrorLevel = 
	pcn := ComObjCreate("ADODB.Connection") 
	pcn.ConnectionString := "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" . DB . ";" 
	pcn.Open() 
	If (pcn.State != 1) 
	{ 
		ErrorLevel := "The connection to the database failed" 
		DB_Error(SQL, ErrorLevel)
	} 
	prs := pcn.Execute(SQL) 
	If ADO_GetError(pcn) 
	{ 
		ErrorLevel := ADO_GetError(pcn) 
		DB_Error(SQL, ErrorLevel)
	} 
	If (prs.BOF) and (prs.EOF) 
	{ 
		ErrorLevel := "The returned RecordSet contains no records" 
		DB_Error(SQL, ErrorLevel)
		Return 
	} 
	While !prs.EOF 
	{ 
		prsFields := prs.Fields 
		Names = 
		Loop, % prsFields.Count ;% 
		{ 
			prsField := prsFields.Item(A_Index-1) 
			Data .= prsField.Value . "|" 
			Names .= prsField.Name . "|" 
		} 
		StringTrimRight, Data, Data, 1 
		Data .= "~" 
		prs.MoveNext() 
	} 
	pcn.Close() 
	StringTrimRight, Names, Names, 1 
	StringTrimRight, Data, Data, 1 
	If (sNames) 
	{ 
		Return (Names . "^" . Data) 
	} 
	Else 
	{ 
		Return Data 
	} 
} 

;===========================================
; Get ay errors directly from the ADO object

ADO_GetError(Conn, Text=1) { 
   ErrorObj := Conn.Errors.Item(0) 
   If !ErrorObj 
   { 
      Return 0 
   } 
   If Text 
   { 
      Return % ErrorObj.Description ;% 
   } 
   Else 
   { 
      Return % ErrorObj.Number ;% 
   } 
}

;===========================================
; Deal with any errors resulting from Database queries

DB_Error(SQL, Error) {
	Global
	DB_Error =
	If Error
	{
		If (Error != "The returned RecordSet contains no records")
		{
			FileAppend, %A_Now% - %SQL% - %Error%`n, DataBase.log
			MsgBox, 20, FieldLogs Lite Desktop Widget, There has been an Error.`n`nError Reads:`n`n%Error%`n`nIf you are worried about this error please click No and contact Sam Griffiths <sam.griffiths@environment-agency.gov.uk>`n(You can press Ctrl+c to copy the contents of this error message and copy it into the e-mail).`n`n`nWould you like to continue?
			IfMsgBox, No
			{
				ExitApp
			}
			Else
			{
				DB_Error = 1
			}
		}
	}
	ErrorLevel = 
	Return
}

