﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
ComObjError(0) ;the database functions sometimes give COM errors. this turns that reporting off
#SingleInstance, force

version = 1.1

;The Application Data flder is the only one on the C: drive of Win7 Toughbooks which is createable and writable. 

ProgramDir := A_AppData . "\FieldLogs Lite\"

FileCreateDir, % ProgramDir
If ErrorLevel
{
	MsgBox, 16, FieldLogs Lite, There was a problem creating the FieldLogs Lite directory.
	ExitApp
}
SetWorkingDir, % ProgramDir

If (A_ScriptFullPath != A_WorkingDir . "\FieldLogs Lite Desktop Widget.exe")
{
	FileMove, %A_ScriptFullPath%, %A_WorkingDir%\FieldLogs Lite Desktop Widget.exe, 1
	Run, %A_WorkingDir%\FieldLogs Lite Desktop Widget.exe
	ExitApp
}

;Create the shortcut on the desktop if not already there.
If !FileExist(A_Desktop . "\FieldLogs Lite Desktop Widget.lnk")
{
	FileCreateShortcut, %A_WorkingDir%\FieldLogs Lite Desktop Widget.exe, %A_Desktop%\FieldLogs Lite Desktop Widget.lnk, %A_WorkingDir%, , FieldLogs Lite Desktop Widget, %A_WorkingDir%\log.ico, F,
}

;Sort out the Icon and Tray Icon
FileInstall, log.ico, %A_WorkingDir%\log.ico, 1
Menu, Tray, Icon, %A_WorkingDir%\log.ico
Menu, Tray, NoStandard
Menu, Tray, Add, Instructions, Instructions
Menu, Tray, Add, Exit, GuiClose

;Also sort out the top menu bar
Menu, FileMenu, Add, E&xit, GuiClose
Menu, HelpMenu, Add, &Instructions, Instructions
Menu, TopMenuBar, Add, &File, :FileMenu
Menu, TopMenuBar, Add, &Help, :HelpMenu

;To make the whole installation of FieldLogs Lite much easier, 
;the desktop widget will copy the installer to any Archer which doesn't have it installed
FileInstall, FieldLogs Lite Installer.exe, %A_WorkingDir%\FieldLogs Lite Installer.exe, 1

;Get the syncFolders
syncFolders := Get_syncFolders()
;Check each one for the existence of the FieldLogs Lite folder
Loop, Parse, syncFolders, |
{
	If !A_LoopField or (A_LoopField = A_Desktop . "\FieldLogs Lite Data Files\")
	{
		Continue
	}
	IfNotExist, % A_LoopField . "FieldLogs Lite"
	{
		FileCopy, %A_WorkingDir%\FieldLogs Lite Installer.exe, % A_LoopField . "FieldLogs Lite Installer.exe", 1
		MsgBox, 64, FieldLogs Lite, Welcome to FieldLogs Lite.`n`nIt doesn't look like FieldLogs Lite is installed on your Archer.`n`nI have copied the Installation file to your sync folder.`n`n`nPlease synchronise your Archer and run the file`n`n'FieldLogs Lite Installation.exe'`n`nwhich can be found in the \My Documents\ folder. ;'
	}
}

;##############################################################################################################
;Set Env Variables ============================================================================================
;##############################################################################################################

iniDir := A_WorkingDir . "\iniFiles\"
If FileExist(iniDir)
{
	FileRemoveDir, %iniDir%, 1
}
FileCreateDir, %iniDir%

;##############################################################################################################
;Build the main Gui ===========================================================================================
;##############################################################################################################

Gui, +Border
Gui, Menu, TopMenuBar
Gui, Font, s12, Helvetica
Gui, Add, Button, w350 x20 y20 h40 gScanPastData, Send data to Archer
Gui, Font, s10
Gui, Add, Text, w330 xp10 yp55 Center R3, 1. Run the round export from the Field Logs database.`n2. Click the button above.`n3. Follow any instructions.
Gui, Show, AutoSize, FieldLogs Lite - Desktop Widget %version%
Return

;ButtonExit:
GuiClose:
;ClearOut the Working Directory
FileRemoveDir, %A_WorkingDir%
ExitApp


;##############################################################################################################
;Send Data to Archer =========================================================================================
;
; The purpose of this routine is to set up the Archer with the latest data so that it is ready for a field visit. 
; It scans the data from the 'recent logs.mdb' database file which stores the historic data of the sites
; It also copies the selected .xml files for the exported round to the Archer
;##############################################################################################################

ScanPastData:

;It is expected that different rounds may be exported to different folders.
;We should therefore allow the user to select the round and recent logs database to use each time

;Ask the user which round they wish to export
MsgBox, 32, FieldLogs Lite, Please find the $round.xml file of the round you want to export to the Archer.
FileSelectFile, round, 3, %A_MyDocuments%, Choose the round to export to your Archer, Field Logs round file ($round.xml)
If ErrorLevel
{
	Return
}
;Using the file path above, split out the directory
SplitPath, round, , roundDir

;Ask the user to locate the recent logs database 
MsgBox, 32, FieldLogs Lite, Please locate the 'recent logs.mdb' database file you want to scan for historic site data
FileSelectFile, DB, 3, %A_MyDocuments%, Locate 'recent logs.mdb' database file, Recent Logs (*.mdb)
If ErrorLevel
{
	Return
}
;Copy the database to the C: drive to make the reads faster
FileCopy, %DB%, recent_logs.mdb, 1
DB := "recent_logs.mdb"

;So that we can show the user how long the scan process took, we make a note of the time here
StartTime := A_TickCount

;Count the xml file in the round so that the progress bar will be the correct length
Loop, % roundDir . "\*.xml"
{
	countXML := A_Index
}

;Create a Progress bar to impart info to the user during the data scan
Gui, 1: +Disabled
Gui, 10: +Border
Gui, 10: Font, s11 Helvetica
Gui, 10: Add, Text, w300 Center vsiteCount, Copying %countXML% round xml files to sync folder
Gui, 10: Add, Progress, w300 c000000 range0-%countXML% vpastDataProg
Gui, 10: Add, Text, w300 Center vSiteID, Preparing
Gui, 10: Show, AutoSize, Progress

;First we copy over the round xml files to the sync Folders
Loop, Parse, syncFolders, |
{
	If !A_LoopField
	{
		Continue
	}
	ASDir :=  A_LoopField . "FieldLogs Lite"
	If !FileExist(ASDir)
	{
		FileCreateDir, %ASDir%
	}
	;Update the progress bar with the name of the folder
	SplitPath, A_LoopField,, Dir
	GuiControl, 10:, SiteID, %Dir%
	GuiControl, 10:, siteCount, Copying %countXML% round xml files to sync folder
	GuiControl, 10:, pastDataProg, 0
	Loop, % roundDir . "\*.xml"
	{
		FileCopy, %A_LoopFileFullPath%, %ASDir%\%A_LoopFileName%, 1
		If ErrorLevel
		{
			MsgBox, 20, FieldLogs Lite, There was a problem copying an xml file`n`n%A_LoopFileFullPath%`n`nWould you like to continue?, 20
			IfMsgBox, No
			{
				GoSub, 10GuiClose
				Return
			}
		}
		GuiControl, 10:, siteCount, % "Copying " . (countXML - A_Index) . " round xml files to sync folder"
		GuiControl, 10:, pastDataProg, % A_Index
	}
}

;Get the full list of Site ID's from the $round.xml file
;Loop through the chosen $round.xml file and get the siteID from the line which has the siteName in it.
sites := ""
Loop, Read, % round
{
	If !InStr(A_LoopReadLine, "siteID")
	{	
		Continue
	}
	;We read through the file line by line and extract the site ID from the xml
	;We use the StrX function for parsing the xml
	;See http://www.autohotkey.com/board/topic/47368-strx-auto-parser-for-xml-html/
	sites .= StrX(A_LoopReadLine, "siteID", 1, 8, "siteName", True, 10, N) . "~"
}
;sites := DB_Read("SELECT DISTINCT fkey_siteID FROM tblRecentFieldLogs;")
;Get a count of the sites so that the progress bar will be the correct length
Loop, Parse, sites, ~
{
	count := A_Index
}
GuiControl, 10:, pastDataProg, 0
GuiControl, 10: +Range0-%count%, pastDataProg
 
;The next task is to convert the historic data from access database into ini files
GuiControl, 10: , siteCount, Scanning past visit data for %count% sites		
GuiControl, 10: , SiteID, Preparing

;Get the table headers once now to save querying each time in the loop below
logs := DB_Read("SELECT * FROM tblRecentFieldLogs;", 1) 
;Separate the table headings from the actual data
StringSplit, log, logs, ^
;and split the headings into an array
StringSplit, logMetaHeaders, log1, |

;Loop through the site IDs one by one`
Loop, Parse, sites, ~
{
	;for speed rather than using IniWrite each time, we build the INI file in memory and write it to the file at the end
	iniContent =
	;take a note of the currently scanned site ID
	SiteID := A_LoopField
	;Update the progress bar to show the user that we are working
	GuiControl, 10:, siteCount, % "Scanning historic visit data for " . (count-A_Index) . " sites"
	GuiControl, 10:, pastDataProg, %A_Index%
	GuiControl, 10:, SiteID, Current site: %SiteID%
	;Get the log meta data (officer etc.) for the current SiteID
	logs := DB_Read("SELECT TOP 3 * FROM tblRecentFieldLogs WHERE fkey_siteID='" . SiteID . "' AND logRoundName NOT LIKE '%NON VISIT%' ORDER BY logDateTime DESC;") 
	;Loop through the data
	Loop, Parse, logs, ~
	{
		;create a new INI section using the loopindex. This should be 1,2 and 3 for the number of visits
		iniContent .= "[" . A_Index . "]`n"
		;separate the current data line into an array 
		StringSplit, logData, A_LoopField, |
		;We now add the log meta data to the <SiteID>.ini file
		;Loop through the meta data and assign the data to it's header
		Loop, %logMetaHeaders0%
		{
			Header := LogMetaHeaders%A_Index%
			;Ignore logID or fkey_siteID as there is no need for the site ID to be in the INi file
			If (Header = "logID") or (Header = "fkey_siteID")
			{
				Continue
			}
			Data := logData%A_Index%
			;Remove new lines and carriage returns from any free text fields so that the INI entry sits on one lione
			If InStr(Data, "Comment")
			{
				StringReplace, Data, Data, `n, , A
				StringReplace, Data, Data, `r, , A
			}
			;As we go, add each data pair to the logID section of the inifile
			iniContent .= Header . "=" . Data . "`n"
		}
		;We now get the actual recorded data for the current logID
		logValues := DB_Read("SELECT ALL * FROM tblRecentLogValues WHERE fkey_logID=" . logData1 . ";")
		;Loop through the data record by record
		Loop, Parse, logValues, ~
		{
			;Split each data line into an array
			StringSplit, logValueData, A_LoopField, |
			;Write the values to the database
			iniContent .= logValueData2 . "=" . logValueData3 . "`n"
		}
	}
	;Save the newly crested ini file for the current Site ID in the ini directory
	FileAppend, %iniContent%, % iniDir . SiteID . ".ini"
}
;Once we reach this point we have a directory in the FieldLogs Lite program directory 
;containing a past visit INI file for each site available in the database
;We need to copy those files to the Sync folders
;First we update the Progress bar to let the user know what we are doing

GuiControl, 10:, pastDataProg, 0
GuiControl, 10:, siteCount, Copying %count% historic data file to sync folder
GuiControl, 10:, SiteID,
;Loop through the sync folders we scanned at the beginning of the script
Loop, Parse, syncFolders, |
{
	If !A_LoopField
	{
		Continue
	}
	;Update the progress bar with the name of the folder
	SplitPath, A_LoopField,, Dir
	GuiControl, 10:, SiteID, %Dir%
	GuiControl, 10:, siteCount, Copying %count% historic data files to sync folder
	GuiControl, 10:, pastDataProg, 0
	;The Active Sync folder listed in the registry isn;t quite the correct format to save the files to
	;We add the necessary file paths so that the ini files get copied to the right place
	ASDataDir := A_LoopField . "FieldLogs Lite\Historic\"				
	;First remove the directory to get rid of any existing INI files then re-create it ready for the new round
	If FileExist(ASDataDir)
	{
		FileRemoveDir, %ASDataDir%, 1
	}
	FileCreateDir, %ASDataDir%
	;Then copy the iniDir to the Data folder in the AS Folder
	;The next time the device associated with the Active sync folder is connected, the files will be copied to it
	Loop, % iniDir . "\*.ini"
	{
		FileCopy, % A_LoopFileFullPath, % ASDataDir . A_LoopFileName, 1
		If ErrorLevel
		{
			MsgBox, 20, FieldLogs Lite, There was a problem copying a historic data file`n`n%A_LoopFileFullPath%`n`nWould you like to continue?, 20
			IfMsgBox, No
			{
				GoSub, 10GuiClose
				Return
			}
		}
		GuiControl, 10:, siteCount, % "Copying " . (count-A_Index) . " historic data files to sync folder"
		GuiControl, 10:, pastDataProg, % A_Index
	}
}

;ClearOut the Working Directory
FileRemoveDir, %A_WorkingDir%

;Get rid of the progress bar
Gui, 1: -Disabled
Gui, 10: Destroy
;Let the user know that the process is complete
;we let them know aproximately how long the process took too.
MsgBox, 64, FieldLogs Lite - Desktop Widget, % "All the data needed for your next field visit was copied in " . Round(((A_TickCount - StartTime)/60000), 3) . " minutes.`n`nPlease synchronise your field device(s) with your Toughbook to ensure you have the latest data"
;Clear up and make sure the main GUI works for the user
Gui, 1: Default 
Gui, 1: Show
Return

10GuiClose:
Gui, 1: -Disabled
Gui, 10: Destroy
Gui, 1: Default
Gui, 1: Show
Return


Instructions:
Run, http://hydro.metry.co.uk/index.php/FieldLogs_Lite
Return

;========================================================
; Functions
;========================================================

Get_syncFolders() {
	Loop, HKEY_CURRENT_USER, Software\Microsoft\Windows CE Services\Partners\, 1, 1
	{
		;By searching for a specific key name in the registry we can build a list of active sync folders
		If (A_LoopRegName = "Briefcase Path")
		{
			;Save the path in the registry as the variable 'Folder'
			RegRead, Folder
			;Test the folder to see if it exists 
			;Some folders are saved a s afull path, some we need to add the A_MyDocuments variable to
			IfNotExist, %Folder%
			{
				Folder := A_MyDocuments . Folder
			}
			;We add the scanned folder to a list of active sync folders
			syncFolders .= Folder . "|"
		}
	}
	;If we haven't found any AS Folders
	If !syncFolders
	{
		;We alert the user
		MsgBox, 16, FieldLogs Lite, I couldn't find a Sync Folder for your Archer.`nFor now, the data files will be saved to a folder on your Desktop. ;'
		syncFolders := A_Desktop . "\FieldLogs Lite Data Files\|"
		IfNotExist, % A_Desktop . "\FieldLogs Lite Data Files\"
		{
			FileCreateDir, % A_Desktop . "\FieldLogs Lite Data Files\"
		}	
	}
	Return syncFolders
}

;StrX is the function we use to parse the xml files
StrX( H,  BS="",BO=0,BT=1,   ES="",EO=0,ET=1,  ByRef N="" ) { ;    | by Skan | 19-Nov-2009
	Return SubStr(H,P:=(((Z:=StrLen(ES))+(X:=StrLen(H))+StrLen(BS)-Z-X)?((T:=InStr(H,BS,0,((BO
 <0)?(1):(BO))))?(T+BT):(X+1)):(1)),(N:=P+((Z)?((T:=InStr(H,ES,0,((EO)?(P+1):(0))))?(T-P+Z
 +(0-ET)):(X+P)):(X)))-P) ; v1.0-196c 21-Nov-2009 www.autohotkey.com/forum/topic51354.html
}
